<?php

include("fonctions.php");

session_start();

if(!isset($_SESSION['login']))
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

if($_SESSION['Administrateur'] == 0)
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

modif_thi();

global $link;

if(isset($_POST) && !empty($_POST['modif_thi'])){

	$num_thi = $_POST['modif_thi'];

	$sql = mysqli_query($link, "SELECT jour_de_la_semaine, horaire_debut, horaire_fin, limite FROM tbl_thi WHERE id = $num_thi");

  	$row = mysqli_fetch_assoc($sql);

  	/* Affiche le jour de la semaine dans le formulaire */
  	$jour_de_la_semaine = $row['jour_de_la_semaine'];
  	
  	$select_lundi = "";
  	if($jour_de_la_semaine == "Lundi"){
  		$select_lundi = "selected";
  	}

  	$select_mardi = "";
  	if($jour_de_la_semaine == "Mardi"){
  		$select_mardi = "selected";
  	}

  	$select_mercredi = "";
  	if($jour_de_la_semaine == "Mercredi"){
  		$select_mercredi = "selected";
  	}

  	$select_jeudi = "";
  	if($jour_de_la_semaine == "Jeudi"){
  		$select_jeudi = "selected";
  	}

  	$select_vendredi = "";
  	if($jour_de_la_semaine == "Vendredi"){
  		$select_vendredi = "selected";
  	}

  	$select_samedi = "";
  	if($jour_de_la_semaine == "Samedi"){
  		$select_samedi = "selected";
  	}

  	$select_dimanche = "";
  	if($jour_de_la_semaine == "Dimanche"){
  		$select_dimanche = "selected";
  	}

  	/* Affiche l'horaire du début dans le formulaire */
  	$horaire_debut 		= $row['horaire_debut'];

  	$selected_7_debut = "";
  	if($horaire_debut == "7:00"){
  		$selected_7_debut = "selected";
  	}

  	$selected_730_debut = "";
  	if($horaire_debut == "7:30"){
  		$selected_730_debut = "selected";
  	}

  	$selected_8_debut = "";
  	if($horaire_debut == "8:00"){
  		$selected_8_debut = "selected";
  	}

  	$selected_830_debut = "";
  	if($horaire_debut == "8:30"){
  		$selected_830_debut = "selected";
  	}

  	$selected_9_debut = "";
  	if($horaire_debut == "9:00"){
  		$selected_9_debut = "selected";
  	}

  	$selected_930_debut = "";
  	if($horaire_debut == "9:30"){
  		$selected_930_debut = "selected";
  	}

  	$selected_10_debut = "";
  	if($horaire_debut == "10:00"){
  		$selected_10_debut = "selected";
  	}

  	$selected_1030_debut = "";
  	if($horaire_debut == "10:30"){
  		$selected_1030_debut = "selected";
  	}

  	$selected_11_debut = "";
  	if($horaire_debut == "11:00"){
  		$selected_11_debut = "selected";
  	}

  	$selected_1130_debut = "";
  	if($horaire_debut == "11:30"){
  		$selected_1130_debut = "selected";
  	}

  	$selected_12_debut = "";
  	if($horaire_debut == "12:00"){
  		$selected_12_debut = "selected";
  	}

  	$selected_1215_debut = "";
  	if($horaire_debut == "12:15"){
  		$selected_1215_debut = "selected";
  	}

  	$selected_1230_debut = "";
  	if($horaire_debut == "12:30"){
  		$selected_1230_debut = "selected";
  	}

  	$selected_13_debut = "";
  	if($horaire_debut == "13:00"){
  		$selected_13_debut = "selected";
  	}

  	$selected_1315_debut = "";
  	if($horaire_debut == "13:15"){
  		$selected_1315_debut = "selected";
  	}

  	$selected_1330_debut = "";
  	if($horaire_debut == "13:30"){
  		$selected_1330_debut = "selected";
  	}

  	$selected_14_debut = "";
  	if($horaire_debut == "14:00"){
  		$selected_14_debut = "selected";
  	}

  	$selected_1430_debut = "";
  	if($horaire_debut == "14:30"){
  		$selected_1430_debut = "selected";
  	}

  	$selected_15_debut = "";
  	if($horaire_debut == "15:00"){
  		$selected_15_debut = "selected";
  	}

  	$selected_1530_debut = "";
  	if($horaire_debut == "15:30"){
  		$selected_1530_debut = "selected";
  	}

  	$selected_16_debut = "";
  	if($horaire_debut == "16:00"){
  		$selected_16_debut = "selected";
  	}

  	$selected_1630_debut = "";
  	if($horaire_debut == "16:30"){
  		$selected_1630_debut = "selected";
  	}

  	$selected_17_debut = "";
  	if($horaire_debut == "17:00"){
  		$selected_17_debut = "selected";
  	}

  	$selected_1730_debut = "";
  	if($horaire_debut == "17:30"){
  		$selected_1730_debut = "selected";
  	}

  	$selected_18_debut = "";
  	if($horaire_debut == "18:00"){
  		$selected_18_debut = "selected";
  	}

  	$selected_1830_debut = "";
  	if($horaire_debut == "18:30"){
  		$selected_1830_debut = "selected";
  	}

  	$selected_19_debut = "";
  	if($horaire_debut == "19:00"){
  		$selected_19_debut = "selected";
  	}

  	$selected_1930_debut = "";
  	if($horaire_debut == "19:30"){
  		$selected_1930_debut = "selected";
  	}

  	$selected_20_debut = "";
  	if($horaire_debut == "20:00"){
  		$selected_20_debut = "selected";
  	}

  	$selected_2030_debut = "";
  	if($horaire_debut == "20:30"){
  		$selected_2030_debut = "selected";
  	}

  	$selected_21_debut = "";
  	if($horaire_debut == "21:00"){
  		$selected_21_debut = "selected";
  	}

  	$selected_2130_debut = "";
  	if($horaire_debut == "21:30"){
  		$selected_2130_debut = "selected";
  	}

  	$selected_22_debut = "";
  	if($horaire_debut == "22:00"){
  		$selected_22_debut = "selected";
  	}

  	$selected_2230_debut = "";
  	if($horaire_debut == "22:30"){
  		$selected_2230_debut = "selected";
  	}

  	/* Affiche l'horaire de fin dans le formulaire */
  	$horaire_fin 		= $row['horaire_fin'];

  	$selected_7_fin = "";
  	if($horaire_fin == "7:00"){
  		$selected_7_fin = "selected";
  	}

  	$selected_730_fin = "";
  	if($horaire_fin == "7:30"){
  		$selected_730_fin = "selected";
  	}

  	$selected_8_fin = "";
  	if($horaire_fin == "8:00"){
  		$selected_8_fin = "selected";
  	}

  	$selected_830_fin = "";
  	if($horaire_fin == "8:30"){
  		$selected_830_fin = "selected";
  	}

  	$selected_9_fin = "";
  	if($horaire_fin == "9:00"){
  		$selected_9_fin = "selected";
  	}

  	$selected_930_fin = "";
  	if($horaire_fin == "9:30"){
  		$selected_930_fin = "selected";
  	}

  	$selected_10_fin = "";
  	if($horaire_fin == "10:00"){
  		$selected_10_fin = "selected";
  	}

  	$selected_1030_fin = "";
  	if($horaire_fin == "10:30"){
  		$selected_1030_fin = "selected";
  	}

  	$selected_11_fin = "";
  	if($horaire_fin == "11:00"){
  		$selected_11_fin = "selected";
  	}

  	$selected_1130_fin = "";
  	if($horaire_fin == "11:30"){
  		$selected_1130_fin = "selected";
  	}

  	$selected_12_fin = "";
  	if($horaire_fin == "12:00"){
  		$selected_12_fin = "selected";
  	}

  	$selected_1215_fin = "";
  	if($horaire_fin == "12:15"){
  		$selected_1215_fin = "selected";
  	}

  	$selected_1230_fin = "";
  	if($horaire_fin == "12:30"){
  		$selected_1230_fin = "selected";
  	}

  	$selected_13_fin = "";
  	if($horaire_fin == "13:00"){
  		$selected_13_fin = "selected";
  	}

  	$selected_1315_fin = "";
  	if($horaire_fin == "13:15"){
  		$selected_1315_fin = "selected";
  	}

  	$selected_1330_fin = "";
  	if($horaire_fin == "13:30"){
  		$selected_1330_fin = "selected";
  	}

  	$selected_14_fin = "";
  	if($horaire_fin == "14:00"){
  		$selected_14_fin = "selected";
  	}

  	$selected_1430_fin = "";
  	if($horaire_fin == "14:30"){
  		$selected_1430_fin = "selected";
  	}

  	$selected_15_fin = "";
  	if($horaire_fin == "15:00"){
  		$selected_15_fin = "selected";
  	}

  	$selected_1530_fin = "";
  	if($horaire_fin == "15:30"){
  		$selected_1530_fin = "selected";
  	}

  	$selected_16_fin = "";
  	if($horaire_fin == "16:00"){
  		$selected_16_fin = "selected";
  	}

  	$selected_1630_fin = "";
  	if($horaire_fin == "16:30"){
  		$selected_1630_fin = "selected";
  	}

  	$selected_17_fin = "";
  	if($horaire_fin == "17:00"){
  		$selected_17_fin = "selected";
  	}

  	$selected_1730_fin = "";
  	if($horaire_fin == "17:30"){
  		$selected_1730_fin = "selected";
  	}

  	$selected_18_fin = "";
  	if($horaire_fin == "18:00"){
  		$selected_18_fin = "selected";
  	}

  	$selected_1830_fin = "";
  	if($horaire_fin == "18:30"){
  		$selected_1830_fin = "selected";
  	}

  	$selected_19_fin = "";
  	if($horaire_fin == "19:00"){
  		$selected_19_fin = "selected";
  	}

  	$selected_1930_fin = "";
  	if($horaire_fin == "19:30"){
  		$selected_1930_fin = "selected";
  	}

  	$selected_20_fin = "";
  	if($horaire_fin == "20:00"){
  		$selected_20_fin = "selected";
  	}

  	$selected_2030_fin = "";
  	if($horaire_fin == "20:30"){
  		$selected_2030_fin = "selected";
  	}

  	$selected_21_fin = "";
  	if($horaire_fin == "21:00"){
  		$selected_21_fin = "selected";
  	}

  	$selected_2130_fin = "";
  	if($horaire_fin == "21:30"){
  		$selected_2130_fin = "selected";
  	}

  	$selected_22_fin = "";
  	if($horaire_fin == "22:00"){
  		$selected_22_fin = "selected";
  	}

  	$selected_2230_fin = "";
  	if($horaire_fin == "22:30"){
  		$selected_2230_fin = "selected";
  	}
  	
  	/* Affiche la limitation du nombre de participants dans le formulaire */
  	$limite = $row['limite'];

}

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $chrg_header; ?>
</head>

	<div class="wrapper">

  		<!-- Main Header -->
		<header class="main-header">
		    <!-- Logo -->
		    <a href="list_thi.php" class="logo">
		    	<!-- mini logo for sidebar mini 50x50 pixels -->
		    	<span class="logo-mini"><b>C</b>R</span>
		    	<!-- logo for regular state and mobile devices -->
		    	<span class="logo-lg"><b>Crossfit</b> Reze</span>
		    </a>
			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">	
	  					<!-- User Account Menu -->
	  					<li class="dropdown user user-menu">
	    					<!-- Menu Toggle Button -->
	    					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      						<!-- The user image in the navbar-->
	      						<img src="dist/img/avatarr.png" class="user-image" alt="User Image">
	      						<!-- hidden-xs hides the username on small devices so only the image appears. -->
	      						<span class="hidden-xs"><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></span>
	    					</a>
	    					<ul class="dropdown-menu">
	      						<!-- The user image in the menu -->
	      						<li class="user-header">
	        						<img src="dist/img/avatarr.png" class="img-circle" alt="User Image">
	        						<p>
	          							<?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?>
	          							<small>Inscrit depuis le <?php echo date("d-m-Y", strtotime($_SESSION['Date_inscription'])); ?></small>
                          <small>Nombre de séance WOD: <?php echo $_SESSION['Thi_card'];?></small>
	        						</p>
	      						</li>
	      						<!-- Menu Footer-->
	      						<li class="user-footer">
	        						<div class="pull-right">
	          							<a href="logout.php" class="btn btn-default btn-flat">Se déconnecter</a>
	        						</div>
	      						</li>
	    					</ul>
	  					</li>
					</ul>
				</div>
			</nav>
		</header>

  		<!-- Left side column. contains the logo and sidebar -->
  		<aside class="main-sidebar">
    		<!-- sidebar: style can be found in sidebar.less -->
    		<section class="sidebar">
      			<!-- Sidebar user panel (optional) -->
      			<div class="user-panel">
        			<div class="pull-left image">
          				<img src="dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        			</div>
        			<div class="pull-left info">
          				<p><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></p>
          				<!-- Status -->
          				<i class="fa fa-circle text-success"></i> En ligne
        			</div>
      			</div>

      			<!-- Sidebar Menu -->
      			<ul class="sidebar-menu">
        			<li class="header">ESPACE ADHÉRENTS</li>
        			<!-- Optionally, you can add icons to the links -->
        			
        			<?php

        			if(($_SESSION['Administrateur'] == 1)){
        				echo "
        				<li class='treeview'>
          				<a href='#''><i class='fa fa-link'></i> <span>Gestion Adhérent</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
          					<li><a href='gestion_adherents.php'>Liste des adhérent</a></li>
            				<li><a href='add_adherent.php'>Ajouter un adhérent</a></li>
          				</ul>
        			</li>
        			<li class='treeview'>
          				<a href='#'><i class='fa fa-link'></i> <span>Gestion WOD</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
            				<li><a href='add_thi.php'>Ajouter un WOD</a></li>
          				</ul>
        			</li>";
        			}
        			
        			?>
        			<li><a href="list_thi.php"><i class="fa fa-link"></i> <span>Réservation WOD</span></a></li>
      			</ul>
      		<!-- /.sidebar-menu -->
    		</section>
    	<!-- /.sidebar -->
  		</aside>

  		<!-- Content Wrapper. Contains page content -->
  		<div class="content-wrapper">
    		<!-- Content Header (Page header) -->
    		<section class="content-header">
      			<h1>
        			Gestion WOD
        			<small>Créer, modifier, supprimer un WOD</small>
      			</h1>
    		</section>

    		<!-- Main content -->
    		<section class="content">
    			<div class="row">
			        <!-- left column -->
			        <div class="col-xs-12">
			        	<!-- general form elements -->
			        	<div class="box box-solid box-info">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Modifier un WOD</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" action="modif_thi.php" method="post">
				              	<div class="box-body">
				                	<div class="form-group">
										<h4>Jour de la semaine :</h4>
										<select class="form-control" name="jour_semaine" required>
											<option></option>
											<option <?php echo $select_lundi;?>>Lundi</option>
											<option <?php echo $select_mardi;?>>Mardi</option>
											<option <?php echo $select_mercredi;?>>Mercredi</option>
											<option <?php echo $select_jeudi;?>>Jeudi</option>
											<option <?php echo $select_vendredi;?>>Vendredi</option>
											<option <?php echo $select_samedi;?>>Samedi</option>
											<option <?php echo $select_dimanche;?>>Dimanche</option>
										</select>
									</div>
				                	<div class="form-group">
				                  		<h4>Horaire du début :</h4>
				                  		<select class="form-control" name="horaire_debut" required>
											<option></option>
											<option <?php echo $selected_7_debut;?>>7:00</option>
											<option <?php echo $selected_730_debut;?>>7:30</option>
											<option <?php echo $selected_8_debut;?>>8:00</option>
											<option <?php echo $selected_830_debut;?>>8:30</option>
											<option <?php echo $selected_9_debut;?>>9:00</option>
											<option <?php echo $selected_930_debut;?>>9:30</option>
											<option <?php echo $selected_10_debut;?>>10:00</option>
											<option <?php echo $selected_1030_debut;?>>10:30</option>
											<option <?php echo $selected_11_debut;?>>11:00</option>
											<option <?php echo $selected_1130_debut;?>>11:30</option>
											<option <?php echo $selected_12_debut;?>>12:00</option>
											<option <?php echo $selected_1215_debut;?>>12:15</option>
											<option <?php echo $selected_1230_debut;?>>12:30</option>
											<option <?php echo $selected_13_debut;?>>13:00</option>
											<option <?php echo $selected_1315_debut;?>>13:15</option>
											<option <?php echo $selected_1330_debut;?>>13:30</option>
											<option <?php echo $selected_14_debut;?>>14:00</option>
											<option <?php echo $selected_1430_debut;?>>14:30</option>
											<option <?php echo $selected_15_debut;?>>15:00</option>
											<option <?php echo $selected_1530_debut;?>>15:30</option>
											<option <?php echo $selected_16_debut;?>>16:00</option>
											<option <?php echo $selected_1630_debut;?>>16:30</option>
											<option <?php echo $selected_17_debut;?>>17:00</option>
											<option <?php echo $selected_1730_debut;?>>17:30</option>
											<option <?php echo $selected_18_debut;?>>18:00</option>
											<option <?php echo $selected_1830_debut;?>>18:30</option>
											<option <?php echo $selected_19_debut;?>>19:00</option>
											<option <?php echo $selected_1930_debut;?>>19:30</option>
											<option <?php echo $selected_20_debut;?>>20:00</option>
											<option <?php echo $selected_2030_debut;?>>20:30</option>
											<option <?php echo $selected_21_debut;?>>21:00</option>
											<option <?php echo $selected_2130_debut;?>>21:30</option>
											<option <?php echo $selected_22_debut;?>>22:00</option>
											<option <?php echo $selected_2230_debut;?>>22:30</option>
										</select>
				                	</div>
				                	<div class="form-group">
				                  		<h4>Horaire de fin :</h4>
				                  		<select class="form-control" name="horaire_fin" required>
				                  			<option></option>
											<option <?php echo $selected_7_fin;?>>7:00</option>
											<option <?php echo $selected_730_fin;?>>7:30</option>
											<option <?php echo $selected_8_fin;?>>8:00</option>
											<option <?php echo $selected_830_fin;?>>8:30</option>
											<option <?php echo $selected_9_fin;?>>9:00</option>
											<option <?php echo $selected_930_fin;?>>9:30</option>
											<option <?php echo $selected_10_fin;?>>10:00</option>
											<option <?php echo $selected_1030_fin;?>>10:30</option>
											<option <?php echo $selected_11_fin;?>>11:00</option>
											<option <?php echo $selected_1130_fin;?>>11:30</option>
											<option <?php echo $selected_12_fin;?>>12:00</option>
											<option <?php echo $selected_1215_fin;?>>12:15</option>
											<option <?php echo $selected_1230_fin;?>>12:30</option>
											<option <?php echo $selected_1315_fin;?>>13:15</option>
											<option <?php echo $selected_13_fin;?>>13:00</option>
											<option <?php echo $selected_1330_fin;?>>13:30</option>
											<option <?php echo $selected_14_fin;?>>14:00</option>
											<option <?php echo $selected_1430_fin;?>>14:30</option>
											<option <?php echo $selected_15_fin;?>>15:00</option>
											<option <?php echo $selected_1530_fin;?>>15:30</option>
											<option <?php echo $selected_16_fin;?>>16:00</option>
											<option <?php echo $selected_1630_fin;?>>16:30</option>
											<option <?php echo $selected_17_fin;?>>17:00</option>
											<option <?php echo $selected_1730_fin;?>>17:30</option>
											<option <?php echo $selected_18_fin;?>>18:00</option>
											<option <?php echo $selected_1830_fin;?>>18:30</option>
											<option <?php echo $selected_19_fin;?>>19:00</option>
											<option <?php echo $selected_1930_fin;?>>19:30</option>
											<option <?php echo $selected_20_fin;?>>20:00</option>
											<option <?php echo $selected_2030_fin;?>>20:30</option>
											<option <?php echo $selected_21_fin;?>>21:00</option>
											<option <?php echo $selected_2130_fin;?>>21:30</option>
											<option <?php echo $selected_22_fin;?>>22:00</option>
											<option <?php echo $selected_2230_fin;?>>22:30</option>
										</select>
				                	</div>
				                	<div class="form-group">
				                		<h4>Inscrire le nombre maximum d'adhérents à ce WOD :</h4>
				                		<?php echo "<input type='number' class='form-control' name='limite' value='{$limite}' required>"; ?>
				                	</div>
				                	<div class="form-group">
				                		<?php echo "<input type='hidden' class='form-control' name='num_thi' value='{$num_thi}' required>"; ?>
				                	</div>
				              	</div>
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-info">Modifier <i class="fa fa-magic"></i></button>
				              	</div>
				            </form>
				      	</div>
						<!-- /.box -->
			        </div>
			    </div>
    		</section>
    		<!-- /.content -->
  		</div>
  		<!-- /.content-wrapper -->
  		<!-- Main Footer -->
  		<footer class="main-footer">
    		<!-- To the right -->
    		<div class="pull-right hidden-xs">
      			Seul, on est fort. Ensemble, on est invincible !
    		</div>
    		<!-- Default to the left -->
    		<strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.crossfit-reze.fr/">Crossfit Reze</a>.</strong> All rights reserved.
  		</footer>
  		
  		<!-- Add the sidebar's background. This div must be placed
       	immediately after the control sidebar -->
  		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<?php echo $chrg_footer; ?>
</body>
</html>