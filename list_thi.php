<?php

include("fonctions.php");

session_start();

if(!isset($_SESSION['login']))
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}


$supprm_thi = delete_thi();
$pop_up = deinscription_thi();
$html = reservation_thi();

/* Lundi de chaque semaine */
$sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
$row3 = mysqli_fetch_array($sql3);
$semaine = $row3['semaine'];

$l = strtotime(date('o-\\W'.$semaine.'')); 

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $chrg_header; ?>
</head>

	<div class="wrapper">

  		<!-- Main Header -->
		<header class="main-header">
		    <!-- Logo -->
		    <a href="list_thi.php" class="logo">
		    	<!-- mini logo for sidebar mini 50x50 pixels -->
		    	<span class="logo-mini"><b>C</b>R</span>
		    	<!-- logo for regular state and mobile devices -->
		    	<span class="logo-lg"><b>Crossfit</b> Reze</span>
		    </a>
			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">	
	  					<!-- User Account Menu -->
	  					<li class="dropdown user user-menu">
	    					<!-- Menu Toggle Button -->
	    					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      						<!-- The user image in the navbar-->
	      						<img src="dist/img/avatarr.png" class="user-image" alt="User Image">
	      						<!-- hidden-xs hides the username on small devices so only the image appears. -->
	      						<span class="hidden-xs"><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></span>
	    					</a>
	    					<ul class="dropdown-menu">
	      						<!-- The user image in the menu -->
	      						<li class="user-header">
	        						<img src="dist/img/avatarr.png" class="img-circle" alt="User Image">
	        						<p>
	          							<?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?>
	          							<small>Inscrit depuis le <?php echo date("d-m-Y", strtotime($_SESSION['Date_inscription'])); ?></small>
	          							<small><?php if($_SESSION['Pack'] != "Silver"){echo "Nombre de séance WOD: '".$_SESSION['Thi_card']."'";}?></small>
	        						</p>
	      						</li>
	      						<!-- Menu Footer-->
	      						<li class="user-footer">
	        						<div class="pull-right">
	          							<a href="logout.php" class="btn btn-default btn-flat">Se déconnecter</a>
	        						</div>
	      						</li>
	    					</ul>
	  					</li>
					</ul>
				</div>
			</nav>
		</header>

  		<!-- Left side column. contains the logo and sidebar -->
  		<aside class="main-sidebar">
    		<!-- sidebar: style can be found in sidebar.less -->
    		<section class="sidebar">
      			<!-- Sidebar user panel (optional) -->
      			<div class="user-panel">
        			<div class="pull-left image">
          				<img src="dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        			</div>
        			<div class="pull-left info">
          				<p><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></p>
          				<!-- Status -->
          				<i class="fa fa-circle text-success"></i> En ligne
        			</div>
      			</div>

      			<!-- Sidebar Menu -->
      			<ul class="sidebar-menu">
        			<li class="header">ESPACE ADHÉRENTS</li>
        			<!-- Optionally, you can add icons to the links -->
        			
        			<?php

        			if(($_SESSION['Administrateur'] == 1)){
        				echo "
        				<li class='treeview'>
          				<a href='#''><i class='fa fa-link'></i> <span>Gestion Adhérent</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
          					<li><a href='gestion_adherents.php'>Liste des adhérent</a></li>
            				<li><a href='add_adherent.php'>Ajouter un adhérent</a></li>
          				</ul>
        			</li>
        			<li class='treeview'>
          				<a href='#'><i class='fa fa-link'></i> <span>Gestion WOD</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
            				<li><a href='add_thi.php'>Ajouter un WOD</a></li>
          				</ul>
        			</li>";
        			}
        			
        			?>
        			<li class="active"><a href="list_thi.php"><i class="fa fa-link"></i> <span>Réservation WOD</span></a></li>
      			</ul>
      		<!-- /.sidebar-menu -->
    		</section>
    	<!-- /.sidebar -->
  		</aside>

  		<!-- Content Wrapper. Contains page content -->
  		<div class="content-wrapper">
    		<!-- Content Header (Page header) -->
    		<section class="content-header">
      			<h1>
        			Retrouvez vos WOD
        			<small>cliquer sur le créneau désiré pour réserver</small>
      			</h1>
    		</section>

    		<!-- Main content -->
    		<section class="content">

      			<div class="row">
        			<div class="col-md-12">
        				<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Lundi <?php echo date('d/m/Y',$l);?></h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				            	<?php echo $html; echo $pop_up; echo $supprm_thi;?>
				              	<table class="table table-bordered">
				                	<tr>
				                  		<th>Heure</th>
				                  		<th>Cours</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "<th>Participants</th>";
				                  		}
				                  		?>
				                  		<th>Nom des participants</th>
				                  		<th style="width: 40px">Réservations</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "
				                  			<th>Modifier</th>
				                  			<th>Supprimer</th>";
				                  		}
				                  		?>
				                	</tr>
				                	<?php echo affichage_thi_lundi(); ?>
				              	</table>
				            </div>
				        </div>
				        <!-- /.box -->
        			</div>
        			<!-- /.col -->
        			<div class="col-md-12">
        				<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Mardi <?php echo date('d/m/Y',strtotime('+1 day', $l));?></h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				            	<table class="table table-bordered">
				                	<tr>
				                  		<th>Heure</th>
				                  		<th>Cours</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "<th>Participants</th>";
				                  		}
				                  		?>
				                  		<th>Nom des participants</th>
				                  		<th style="width: 40px">Réservations</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "
				                  			<th>Modifier</th>
				                  			<th>Supprimer</th>";
				                  		}
				                  		?>
				                	</tr>
				                	<?php echo affichage_thi_mardi(); ?>
				              	</table>
				            </div>
				        </div>
				        <!-- /.box -->
        			</div>
        			<!-- /.col -->
        			<div class="col-md-12">
        				<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Mercredi <?php echo date('d/m/Y',strtotime('+2 day', $l));?></h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				              	<table class="table table-bordered">
				                	<tr>
				                  		<th>Heure</th>
				                  		<th>Cours</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "<th>Participants</th>";
				                  		}
				                  		?>
				                  		<th>Nom des participants</th>
				                  		<th style="width: 40px">Réservations</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "
				                  			<th>Modifier</th>
				                  			<th>Supprimer</th>";
				                  		}
				                  		?>
				                	</tr>
				                	<?php echo affichage_thi_mercredi(); ?>
				              	</table>
				            </div>
				        </div>
				        <!-- /.box -->
        			</div>
        			<!-- /.col -->
        			<div class="col-md-12">
        				<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Jeudi <?php echo date('d/m/Y',strtotime('+3 day', $l));?></h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				              	<table class="table table-bordered">
				                	<tr>
				                  		<th>Heure</th>
				                  		<th>Cours</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "<th>Participants</th>";
				                  		}
				                  		?>
				                  		<th>Nom des participants</th>
				                  		<th style="width: 40px">Réservations</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "
				                  			<th>Modifier</th>
				                  			<th>Supprimer</th>";
				                  		}
				                  		?>
				                	</tr>
				                	<?php echo affichage_thi_jeudi(); ?>
				              	</table>
				            </div>
				        </div>
				        <!-- /.box -->
        			</div>
        			<!-- /.col -->
        			<div class="col-md-12">
        				<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Vendredi <?php echo date('d/m/Y',strtotime('+4 day', $l));?></h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				              	<table class="table table-bordered">
				                	<tr>
				                  		<th>Heure</th>
				                  		<th>Cours</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "<th>Participants</th>";
				                  		}
				                  		?>
				                  		<th>Nom des participants</th>
				                  		<th style="width: 40px">Réservations</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "
				                  			<th>Modifier</th>
				                  			<th>Supprimer</th>";
				                  		}
				                  		?>
				                	</tr>
				                	<?php echo affichage_thi_vendredi(); ?>
				              	</table>
				            </div>
				        </div>
				        <!-- /.box -->
        			</div>
        			<!-- /.col -->
        			<div class="col-md-12">
        				<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Samedi <?php echo date('d/m/Y',strtotime('+5 day', $l));?></h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				              	<table class="table table-bordered">
				                	<tr>
				                  		<th>Heure</th>
				                  		<th>Cours</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "<th>Participants</th>";
				                  		}
				                  		?>
				                  		<th>Nom des participants</th>
				                  		<th style="width: 40px">Réservations</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "
				                  			<th>Modifier</th>
				                  			<th>Supprimer</th>";
				                  		}
				                  		?>
				                	</tr>
				                	<?php echo affichage_thi_samedi(); ?>
				              	</table>
				            </div>
				        </div>
				        <!-- /.box -->
        			</div>
        			<!-- /.col -->
        			<div class="col-md-12">
        				<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Dimanche <?php echo date('d/m/Y',strtotime('+6 day', $l));?></h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				              	<table class="table table-bordered">
				                	<tr>
				                  		<th>Heure</th>
				                  		<th>Cours</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "<th>Participants</th>";
				                  		}
				                  		?>
				                  		<th>Nom des participants</th>
				                  		<th style="width: 40px">Réservations</th>
				                  		<?php
				                  		if(($_SESSION['Administrateur'] == 1)){
				                  		echo "
				                  			<th>Modifier</th>
				                  			<th>Supprimer</th>";
				                  		}
				                  		?>
				                	</tr>
				                	<?php echo affichage_thi_dimanche(); ?>
				              	</table>
				            </div>
				        </div>
				        <!-- /.box -->
        			</div>
        			<!-- /.col -->
        		</div>
        		<!-- /.row -->
    		</section>
    	<!-- /.content -->
  		</div>
  		<!-- /.content-wrapper -->

  		<!-- Main Footer -->
  		<footer class="main-footer">
    		<!-- To the right -->
    		<div class="pull-right hidden-xs">
      			Seul, on est fort. Ensemble, on est invincible !
    		</div>
    		<!-- Default to the left -->
    		<strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.crossfit-reze.fr/">Crossfit Reze</a>.</strong> All rights reserved.
  		</footer>

  
  		<!-- Add the sidebar's background. This div must be placed
       	immediately after the control sidebar -->
  		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<?php echo $chrg_footer; ?>
</body>
</html>
