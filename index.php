<?php

include("fonctions.php");

$html = form_connexion();

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $chrg_header; ?>
</head>
<body class="skin-black sidebar-mini">
	<div class="wrapper">

  		<!-- Main Header -->
		<header class="main-header">
		    <!-- Logo -->
		    <a href="index.php" class="logo">
		    	<!-- mini logo for sidebar mini 50x50 pixels -->
		    	<span class="logo-mini"><b>C</b>R</span>
		    	<!-- logo for regular state and mobile devices -->
		    	<span class="logo-lg"><b>Crossfit</b> Reze</span>
		    </a>
			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				
			</nav>
		</header>

  		<!-- Left side column. contains the logo and sidebar -->
  		<aside class="main-sidebar">
    		<!-- sidebar: style can be found in sidebar.less -->
    		<section class="sidebar">
      			<!-- Sidebar user panel (optional) -->
      			<div class="user-panel">
        			<div class="pull-left image">
          				
        			</div>
      			</div>

      			<!-- Sidebar Menu -->
      			<ul class="sidebar-menu">
        			<li class="header">ESPACE ADHÉRENTS</li>
        			
      			</ul>
      		<!-- /.sidebar-menu -->
    		</section>
    	<!-- /.sidebar -->
  		</aside>

  		<!-- Content Wrapper. Contains page content -->
  		<div class="content-wrapper">
    		<!-- Content Header (Page header) -->
    		<section class="content-header">
      			<h1>
              <center>
              <img src="dist/img/avatarr.png" class="img-circle" alt="User Image">
      				Espace Adhérents
        			<small>connectez-vous à votre espace en ligne</small>
              </center>
      			</h1>
    		</section>
    		<!-- Main content -->
    		<section class="content">
    			<div class="row">
    				<div class="col-md-6 col-md-offset-3">
						<form action="index.php" method="post">
							<h4>Identifiant :</h4>
							<input type="text" name="login-id" id="login-id" class="form-control" placeholder="Identifiant"  autofocus>
							<h4>Mot de passe :</h4>
							<input type="password" name="login-mdp" id="login-mdp" class="form-control" placeholder="Mot de passe" >
							<h4></h4>
							<button class="btn btn-lg btn-warning btn-block" type="submit">Se connecter</button>
						</form>
						<?php echo $html; ?>
					</div>
				</div>
    		</section>
    	<!-- /.content -->
  		</div>
  		<!-- /.content-wrapper -->

  		<!-- Main Footer -->
  		<footer class="main-footer">
    		<!-- To the right -->
    		<div class="pull-right hidden-xs">
      			Seul, on est fort. Ensemble, on est invincible !
    		</div>
    		<!-- Default to the left -->
    		<strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.crossfit-reze.fr/">Crossfit Reze</a>.</strong> All rights reserved.
  		</footer>

  
  		<!-- Add the sidebar's background. This div must be placed
       	immediately after the control sidebar -->
  		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<?php echo $chrg_footer; ?>
</body>
</html>
