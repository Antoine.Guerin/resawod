<?php

include("fonctions.php");

session_start();

if(!isset($_SESSION['login']))
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

if($_SESSION['Administrateur'] == 0)
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}


modif_adherent();

global $link;

if(isset($_POST) && !empty($_POST['modif_adherent'])){

  $num_adherent = $_POST['modif_adherent'];

  $sql = mysqli_query($link, "SELECT Nom, Prenom, Adresse, CP, Ville, Telephone, Adresse_mail, Mode_de_paiement, Paye, Pack, id, Thi_card, Abonnement FROM tbl_users WHERE id = $num_adherent");

  $row = mysqli_fetch_assoc($sql);
  
  $nom          = $row['Nom'];
  $prenom       = $row['Prenom'];
  $adresse      = $row['Adresse'];
  $CP           = $row['CP'];
  $ville        = $row['Ville'];
  $telephone    = $row['Telephone'];
  $adresse_mail = $row['Adresse_mail'];
  $pack         = $row['Pack'];
  $mode         = $row['Mode_de_paiement'];
  $paye         = $row['Paye'];
  $id           = $row['id'];
  $thi          = $row['Thi_card'];
  $Abonnement   = $row['Abonnement'];

    // Vérifie le pack de l'adhérent

    $pack_bronze  = "";
    $pack_silver  = "";
    $pack_gold    = "";

    if($pack == "Bronze"){
      $pack_bronze = "checked";
    }
    if($pack == "Silver"){
      $pack_silver = "checked";
    }
    if($pack == "Gold"){
      $pack_gold = "checked";
    }

    // Vérifie le mode de paiement de l'adhérent

    $mode_cb      = "";
    $mode_chq     = "";
    $mode_esp     = "";
    $mode_paypal  = "";

    if($mode == "CB"){
      $mode_cb = "checked";
    }
    if($mode == "Chèque"){
      $mode_chq = "checked";
    }
    if($mode == "Espèces"){
      $mode_esp = "checked";
    }
    if($mode == "Paypal"){
      $mode_paypal = "checked";
    }

    // Vérifie si l'abonnement est payé

    $paye_oui = "";
    $paye_non = "";

    if($paye == "Oui"){
      $paye_oui = "checked";
    }
    if($paye == "Non"){
      $paye_non = "checked";
    }

    // Vérifie quel abonnement l'adhérent dispose

    $abonnement_1 = "";
    $abonnement_3 = "";
    $abonnement_6 = "";
    $abonnement_12 = "";

    if($Abonnement == 1){
      $abonnement_1 = "checked";
    }
    if($Abonnement == 3){
      $abonnement_3 = "checked";
    }
    if($Abonnement == 6){
      $abonnement_6 = "checked";
    }
    if($Abonnement == 12){
      $abonnement_12 = "checked";
    }

}


?>
<!DOCTYPE html>
<html>
<head>
  <?php echo $chrg_header; ?>
</head>

  <div class="wrapper">

      <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="list_thi.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>R</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Crossfit</b> Reze</span>
        </a>
      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav"> 
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- The user image in the navbar-->
                    <img src="dist/img/avatarr.png" class="user-image" alt="User Image">
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs"><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                      <img src="dist/img/avatarr.png" class="img-circle" alt="User Image">
                      <p>
                          <?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?>
                          <small>Inscrit depuis le <?php echo date("d-m-Y", strtotime($_SESSION['Date_inscription'])); ?></small>
                          <small>Nombre de séance WOD: <?php echo $_SESSION['Thi_card'];?></small>
                      </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                      <div class="pull-right">
                          <a href="logout.php" class="btn btn-default btn-flat">Se déconnecter</a>
                      </div>
                    </li>
                </ul>
              </li>
          </ul>
        </div>
      </nav>
    </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
              <div class="pull-left image">
                  <img src="dist/img/user2-160x160.png" class="img-circle" alt="User Image">
              </div>
              <div class="pull-left info">
                  <p><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></p>
                  <!-- Status -->
                  <i class="fa fa-circle text-success"></i> En ligne
              </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
              <li class="header">ESPACE ADHÉRENTS</li>
              <!-- Optionally, you can add icons to the links -->

              <?php

              if(($_SESSION['Administrateur'] == 1)){
                echo "
                <li class='treeview active'>
                  <a href='#''><i class='fa fa-link'></i> <span>Gestion Adhérent</span>
                    <span class='pull-right-container'>
                        <i class='fa fa-angle-left pull-right'></i>
                    </span>
                  </a>
                  <ul class='treeview-menu'>
                    <li class='active'><a href='gestion_adherents.php'>Liste des adhérent</a></li>
                    <li><a href='add_adherent.php'>Ajouter un adhérent</a></li>
                  </ul>
              </li>
              <li class='treeview'>
                  <a href='#'><i class='fa fa-link'></i> <span>Gestion WOD</span>
                    <span class='pull-right-container'>
                        <i class='fa fa-angle-left pull-right'></i>
                    </span>
                  </a>
                  <ul class='treeview-menu'>
                    <li><a href='add_thi.php'>Ajouter un WOD</a></li>
                  </ul>
              </li>";
              }
              
              ?>
              <li><a href="list_thi.php"><i class="fa fa-link"></i> <span>Réservation WOD</span></a></li>
            </ul>
          <!-- /.sidebar-menu -->
        </section>
      <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
              Gestion Adhérents
              <small>Créer, modifier, supprimer un adhérent</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
              <!-- left column -->
              <div class="col-xs-12">
                <!-- general form elements -->
                <div class="box box-solid box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Modifier un adhérent</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="modif_adherent.php" method="post">
                        <div class="box-body">
                          <div class="form-group">
                            <?php echo "<input type='hidden' name='Numadherent' value='{$id}'>" ?>
                          </div>
                          <div class="form-group">
                            <h4>Nom de famille :</h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php echo "<input type='text' class='form-control' name='form_name' id='form_name' value='{$nom}' required>"?>
                              </div>
                          </div>
                          <div class="form-group">
                              <h4>Prénom :</h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php echo "<input type='text' class='form-control' name='form_prenom' id='form_prenom' value='{$prenom}' required>"?>
                              </div>
                          </div>
                          <div class="form-group">
                              <h4>Adresse :</h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                <?php echo "<input type='text' class='form-control' name='form_address' id='form_address' value='{$adresse}' required>"?>
                              </div>
                          </div>
                          <div class="form-group">
                              <h4>CP :</h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                <?php echo "<input type='number' class='form-control' name='form_cp' id='form_cp' value='{$CP}' required>"?>
                              </div>
                          </div>
                          <div class="form-group">
                              <h4>Ville :</h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                <?php echo "<input type='text' class='form-control' name='form_city' id='form_city' value='{$ville}' required>"?>
                              </div>
                          </div>
                          <div class="form-group">
                              <h4>Téléphone : <small>(format: xx.xx.xx.xx.xx)</small></h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <?php echo "<input type='text' class='form-control' name='form_telephone' id='form_telephone' pattern='^\d{2}.\d{2}.\d{2}.\d{2}.\d{2}$' value='{$telephone}' required>"?>
                              </div>
                          </div>
                          <div class="form-group">
                              <h4>Adresse mail :</h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo "<input type='email' class='form-control' name='form_email' id='form_email' value='{$adresse_mail}' required>"?>
                              </div>
                          </div>
                          <div class="form-group">
                              <h4>Nombre de séance THI :</h4>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-diamond"></i></span>
                                <?php echo "<input type='number' class='form-control' name='thi_card' id='thi_card' value='{$thi}'>"?>
                              </div>
                          </div>
                          <div class="form-group">
                            <h4>Pack :</h4>
                              <div class="radio">
                                <label class="label label-success">
                                    <input type="radio" name="optionspack" id="form-bronze" value="Bronze" <?php echo $pack_bronze; ?>>
                                    Bronze <i class="fa fa-trophy"></i>
                                </label>
                              </div>
                              <div class="radio">
                                <label class="label label-default">
                                    <input type="radio" name="optionspack" id="form-silver" value="Silver" <?php echo $pack_silver; ?>>
                                    Silver <i class="fa fa-trophy"></i>
                                </label>
                              </div>
                              <div class="radio">
                                <label class="label label-warning">
                                    <input type="radio" name="optionspack" id="form-gold" value="Gold" <?php echo $pack_gold; ?>>
                                    Gold <i class="fa fa-trophy"></i>
                                </label>
                              </div>
                          </div>
                          <div class="form-group">
                            <h4>Durée Abonnement :</h4>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" id="form_1" value="1" <?php echo $abonnement_1; ?>>
                                    1 mois
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" id="form_3" value="3" <?php echo $abonnement_3; ?>>
                                    3 mois
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" id="form-6" value="6" <?php echo $abonnement_6; ?>>
                                    6 mois
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" id="form-12" value="12" <?php echo $abonnement_12; ?>>
                                    12 mois
                                </label>
                              </div>
                          </div>
                          <div class="form-group">
                            <h4>Mode de paiements :</h4>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="optionsmode" id="form-cb" value="CB" <?php echo $mode_cb; ?>>
                                    Carte Bancaire <i class="fa fa-credit-card"></i>
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="optionsmode" id="form-chq" value="Chèque" <?php echo $mode_chq; ?>>
                                    Chèque <i class="fa fa-google-wallet"></i>
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="optionsmode" id="form-especes" value="Espèces" <?php echo $mode_esp; ?>>
                                    Espèces <i class="fa fa-money"></i>
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="optionsmode" id="form-paypal" value="Paypal" <?php echo $mode_paypal; ?>>
                                    Paypal <i class="fa fa-paypal"></i>
                                </label>
                              </div>
                          </div>
                          <div class="form-group">
                            <h4>Abonnement payé ?</h4>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="optionspaye" id="form-yes" value="Oui" <?php echo $paye_oui; ?>>
                                    Oui
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="optionspaye" id="form-no" value="Non" <?php echo $paye_non; ?>>
                                    Non
                                </label>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                          <button type="submit" class="btn btn-info">Modifier <i class="fa fa-magic"></i></button>
                        </div>
                    </form>
                </div>
            <!-- /.box -->
              </div>
          </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Seul, on est fort. Ensemble, on est invincible !
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.crossfit-reze.fr/">Crossfit Reze</a>.</strong> All rights reserved.
      </footer>
      <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>