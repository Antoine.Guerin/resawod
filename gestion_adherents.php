<?php

include("fonctions.php");

session_start();

if(!isset($_SESSION['login']))
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

if($_SESSION['Administrateur'] == 0)
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

$pop_up_renew = renew_adherent();
$html = delete_adherent();

export_csv();

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $chrg_header; ?>
</head>

	<div class="wrapper">

  		<!-- Main Header -->
		<header class="main-header">
		    <!-- Logo -->
		    <a href="list_thi.php" class="logo">
		    	<!-- mini logo for sidebar mini 50x50 pixels -->
		    	<span class="logo-mini"><b>C</b>R</span>
		    	<!-- logo for regular state and mobile devices -->
		    	<span class="logo-lg"><b>Crossfit</b> Reze</span>
		    </a>
			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">	
	  					<!-- User Account Menu -->
	  					<li class="dropdown user user-menu">
	    					<!-- Menu Toggle Button -->
	    					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      						<!-- The user image in the navbar-->
	      						<img src="dist/img/avatarr.png" class="user-image" alt="User Image">
	      						<!-- hidden-xs hides the username on small devices so only the image appears. -->
	      						<span class="hidden-xs"><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></span>
	    					</a>
	    					<ul class="dropdown-menu">
	      						<!-- The user image in the menu -->
	      						<li class="user-header">
	        						<img src="dist/img/avatarr.png" class="img-circle" alt="User Image">
	        						<p>
	          							<?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?>
	          							<small>Inscrit depuis le <?php echo date("d-m-Y", strtotime($_SESSION['Date_inscription'])); ?></small>
                          <small>Nombre de séance WOD: <?php echo $_SESSION['Thi_card'];?></small>
	        						</p>
	      						</li>
	      						<!-- Menu Footer-->
	      						<li class="user-footer">
	        						<div class="pull-right">
	          							<a href="logout.php" class="btn btn-default btn-flat">Se déconnecter</a>
	        						</div>
	      						</li>
	    					</ul>
	  					</li>
					</ul>
				</div>
			</nav>
		</header>

  		<!-- Left side column. contains the logo and sidebar -->
  		<aside class="main-sidebar">
    		<!-- sidebar: style can be found in sidebar.less -->
    		<section class="sidebar">
      			<!-- Sidebar user panel (optional) -->
      			<div class="user-panel">
        			<div class="pull-left image">
          				<img src="dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        			</div>
        			<div class="pull-left info">
          				<p><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></p>
          				<!-- Status -->
          				<i class="fa fa-circle text-success"></i> En ligne
        			</div>
      			</div>

      			<!-- Sidebar Menu -->
      			<ul class="sidebar-menu">
        			<li class="header">ESPACE ADHÉRENTS</li>
        			<!-- Optionally, you can add icons to the links -->

        			<?php

        			if(($_SESSION['Administrateur'] == 1)){
        				echo "
        				<li class='treeview active'>
          				<a href='#''><i class='fa fa-link'></i> <span>Gestion Adhérent</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
          					<li class='active'><a href='gestion_adherents.php'>Liste des adhérent</a></li>
            				<li><a href='add_adherent.php'>Ajouter un adhérent</a></li>
          				</ul>
        			</li>
        			<li class='treeview'>
          				<a href='#'><i class='fa fa-link'></i> <span>Gestion WOD</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
            				<li><a href='add_thi.php'>Ajouter un WOD</a></li>
          				</ul>
        			</li>";
        			}
        			
        			?>
        			<li><a href="list_thi.php"><i class="fa fa-link"></i> <span>Réservation WOD</span></a></li>
      			</ul>
      		<!-- /.sidebar-menu -->
    		</section>
    	<!-- /.sidebar -->
  		</aside>

  		<!-- Content Wrapper. Contains page content -->
  		<div class="content-wrapper">
    		<!-- Content Header (Page header) -->
    		<section class="content-header">
      			<h1>
        			Gestion Adhérents
        			<small>Créer, modifier, supprimer un adhérent</small>
      			</h1>
    		</section>

    		<!-- Main content -->
    		<section class="content">
    			<div class="info-box bg-yellow">
					<span class="info-box-icon"><i class="fa fa-users"></i></span>
				  	<div class="info-box-content">
				    	<span class="info-box-text">Nombre d'adhérents</span>
				    	<span class="info-box-number"><?php echo nombre_adherents(); ?></span>
				    	<!-- The progress section is optional -->
				    	<div class="progress">
				      		<div class="progress-bar" style="width: 70%"></div>
				    	</div>
				    	<span class="progress-description">
				      		Dernière inscription le <?php echo derniere_incription(); ?>
				    	</span>
				  	</div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
      			<div class="row">
        			<div class="col-xs-12">
          				<div class="box box-solid box-success">
            				<div class="box-header">
              					<h3 class="box-title">Liste des adhérents</h3>
            				</div>
            				<!-- /.box-header -->
                    <?php echo $html;?>
                    <?php echo $pop_up_renew;?>
	            			<div class="box-body table-responsive no-padding">
				              	<table class="table table-striped">
				                	<tr>
				                		<th>Prénom</th>
				                  		<th>Nom</th>
				                  		<th>Adresse</th>
				                  		<th>CP</th>
				                  		<th>Ville</th>
				                  		<th>Téléphone</th>
				                  		<th>Adresse Mail</th>
				                  		<th>Identifiant</th>
				                  		<th>Mot de passe</th>
                              <th>Séances WOD restante</th>
				                  		<th>Pack</th>
                              <th>Durée de l'abonnement</th>
				                  		<th>Mode de paiement</th>
				                  		<th >Abonnement Payé ?</th>
				                  		<th>Inscrit depuis le</th>
				                  		<th>Fin de contrat le</th>
				                  		<th></th>
				                  		<th></th>
				                  		<th></th>
				                	</tr>
		                			<?php echo list_adhérents(); ?>
				              	</table>
	            			</div>
	            			<!-- /.box-body -->
          				</div>
          				<!-- /.box -->
                  <form action='gestion_adherents.php' method='post'>
                    <input type='hidden' name='export_adherents' id='export_adherents' value='plop'>
                    <button class='btn btn-info btn-sm' type='submit'> Exporter la liste des adhérents</button>
                  </form>
        			</div>
      			</div>
    		</section>
    		<!-- /.content -->
  		</div>
  		<!-- /.content-wrapper -->
  		<!-- Main Footer -->
  		<footer class="main-footer">
    		<!-- To the right -->
    		<div class="pull-right hidden-xs">
      			Seul, on est fort. Ensemble, on est invincible !
    		</div>
    		<!-- Default to the left -->
    		<strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.crossfit-reze.fr/">Crossfit Reze</a>.</strong> All rights reserved.
  		</footer>

  
  		<!-- Add the sidebar's background. This div must be placed
       	immediately after the control sidebar -->
  		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<?php echo $chrg_footer; ?>
</body>
</html>
