<?php

/***
 *            _       _           _ 
 *       __ _| | ___ | |__   __ _| |
 *      / _` | |/ _ \| '_ \ / _` | |
 *     | (_| | | (_) | |_) | (_| | |
 *      \__, |_|\___/|_.__/ \__,_|_|
 *      |___/                       
 */

/* PARAMETRES CONNEXION BDD */

$bdd_hote 		= "";
$bdd_user 		= "";
$bdd_pwd		= "";
$bdd_selected 	= "";

$link = mysqli_connect($bdd_hote, $bdd_user, $bdd_pwd, $bdd_selected);

/* CHARGEMENT HEADER */
$chrg_header = "
	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta http-equiv='Refresh' content='300'>
	<title>Crossfit Reze | Espace Adhérents</title>
	<link rel='icon' type='image/png' href='favicon.png' />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- Bootstrap 3.3.6 -->
	<link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>
	<!-- Font Awesome -->
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'>
	<!-- Ionicons -->
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'>
	<!-- Theme style -->
	<link rel='stylesheet' href='dist/css/AdminLTE2.css'>

	<link rel='stylesheet' href='dist/css/skins/_all-skins.min.css'>
	<!-- SweetAlert -->
	<link rel='stylesheet' href='dist/css/sweetalert.css'>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src='https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'></script>
	<script src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js'></script>
	<![endif]-->
	<!-- SweetAlert -->
	<script src='dist/js/sweetalert.min.js'></script>
	<body class='hold-transition skin-black sidebar-mini sidebar-collapse'>";


/* CHARGEMENT FOOTER */
$chrg_footer = "
	<!-- jQuery 2.2.3 -->
	<script src='plugins/jQuery/jquery-2.2.3.min.js'></script>
	<!-- Bootstrap 3.3.6 -->
	<script src='bootstrap/js/bootstrap.min.js'></script>
	<!-- AdminLTE App -->
	<script src='dist/js/app.min.js'></script>";

/***
 *      _____ ___  ____  __  __ _   _ _        _    ___ ____  _____   ____  _____    ____ ___  _   _ _   _ _______  _____ ___  _   _ 
 *     |  ___/ _ \|  _ \|  \/  | | | | |      / \  |_ _|  _ \| ____| |  _ \| ____|  / ___/ _ \| \ | | \ | | ____\ \/ /_ _/ _ \| \ | |
 *     | |_ | | | | |_) | |\/| | | | | |     / _ \  | || |_) |  _|   | | | |  _|   | |  | | | |  \| |  \| |  _|  \  / | | | | |  \| |
 *     |  _|| |_| |  _ <| |  | | |_| | |___ / ___ \ | ||  _ <| |___  | |_| | |___  | |__| |_| | |\  | |\  | |___ /  \ | | |_| | |\  |
 *     |_|   \___/|_| \_\_|  |_|\___/|_____/_/   \_\___|_| \_\_____| |____/|_____|  \____\___/|_| \_|_| \_|_____/_/\_\___\___/|_| \_|
 *                                                                                                                                   
 */

/* Vérification de l'indentifiant et du mot de passe du formulaire */
function form_connexion(){
	
	global $link;

	$html = "";

	if(isset($_POST) && !empty($_POST['login-id']) && !empty($_POST['login-mdp'])){
		$sql = mysqli_query($link, "SELECT Mot_de_passe FROM tbl_users WHERE Identifiant = '" . $_POST['login-id'] . "'");
		$row = mysqli_fetch_assoc($sql);
		if($row['Mot_de_passe'] != $_POST['login-mdp'])
		{
			$html = "<script>swal('', 'L\'identifiant ou le mot de passe est incorrect !', 'error');</script>";
		}
		else
		{
			
			$sql = mysqli_query($link, "SELECT id, Administrateur, Nom, Prenom, Adresse, CP, Ville, Telephone, Adresse_mail, Date_inscription, Pack, Thi_card, Abonnement FROM tbl_users WHERE Identifiant = '" . $_POST['login-id'] . "'");
			$row = mysqli_fetch_assoc($sql);

			$abonnement = "+{$row['Abonnement']} month";

			$fin_de_contrat = strtotime(date("d-m-Y", strtotime($abonnement,strtotime($row['Date_inscription']))));
			$aujourdhui = strtotime(date("d-m-Y"));

			$sql = mysqli_query($link, "SELECT * FROM tbl_reservations WHERE id_adherent = '".$row['id']."'");
			$resa = mysqli_num_rows($sql);

			if($fin_de_contrat <= $aujourdhui){
				$html = "<script>swal('', 'Votre contrat est expiré !', 'error');</script>";
			}

			if($row['Pack'] != 'Silver' && $row['Thi_card'] == 0 && $resa == 0 && $row['Administrateur'] == 0){
				$html = "<script>swal('', 'Vous n\'avez plus de séance disponible !', 'error');</script>";
			}

			else{

				session_start();

				$_SESSION['id'] 				= $row['id'];
				$_SESSION['Administrateur'] 	= $row['Administrateur'];
				$_SESSION['Nom'] 				= $row['Nom'];
				$_SESSION['Prenom'] 			= $row['Prenom'];
				$_SESSION['Adresse'] 			= $row['Adresse'];
				$_SESSION['CP'] 				= $row['CP'];
				$_SESSION['Ville'] 				= $row['Ville'];
				$_SESSION['Telephone'] 			= $row['Telephone'];
				$_SESSION['Adresse_mail'] 		= $row['Adresse_mail'];
				$_SESSION['Date_inscription']	= $row['Date_inscription'];
				$_SESSION['login'] 				= $_POST['login-id'];
				$_SESSION['Thi_card']			= $row['Thi_card'];
				$_SESSION['Abonnement']			= $row['Abonnement'];
				$_SESSION['Pack']				= $row['Pack'];

				header("location: list_thi.php" ) ;	

			}
		}
	}

	return $html;
}

/***
 *       ____ _____ ____ _____ ___ ___  _   _      _    ____  _   _ _____ ____  _____ _   _ _____ ____  
 *      / ___| ____/ ___|_   _|_ _/ _ \| \ | |    / \  |  _ \| | | | ____|  _ \| ____| \ | |_   _/ ___| 
 *     | |  _|  _| \___ \ | |  | | | | |  \| |   / _ \ | | | | |_| |  _| | |_) |  _| |  \| | | | \___ \ 
 *     | |_| | |___ ___) || |  | | |_| | |\  |  / ___ \| |_| |  _  | |___|  _ <| |___| |\  | | |  ___) |
 *      \____|_____|____/ |_| |___\___/|_| \_| /_/   \_\____/|_| |_|_____|_| \_\_____|_| \_| |_| |____/ 
 *                                                                                                      
 */

/* calcul le nombre d'adhérents */
function nombre_adherents(){
	
	global $link;
	
	$sql = mysqli_query($link, "SELECT * FROM tbl_users");
	$nombre_dadherent = mysqli_num_rows($sql);
	echo ($nombre_dadherent-2);
}

/* calcul la dernière inscription */
function derniere_incription(){

	global $link;

	$sql = mysqli_query($link, "SELECT Date_inscription FROM tbl_users order by Date_inscription desc limit 0, 1");
	$row = mysqli_fetch_assoc($sql);;
	$derniere_incription = date("d-m-Y", strtotime($row['Date_inscription']));
	echo $derniere_incription;
}

/* Liste complète des adhérents */
function list_adhérents(){

	global $link;

	$sql = mysqli_query($link, "SELECT Id, Nom, Prenom, Adresse, CP, Ville, Telephone, Adresse_mail, Mode_de_paiement, Paye, Pack, Date_inscription, Mot_de_passe, Identifiant, Thi_card, Abonnement FROM tbl_users");

	while($row = mysqli_fetch_array($sql)){


		// Change la couleur suivant les packs des adhérents
		$label = "";

		if ($row['Pack'] == 'Bronze'){
			$label = "success";
		}

		if ($row['Pack'] == 'Silver'){
			$label = "default";
		}

		if ($row['Pack'] == 'Gold'){
			$label = "warning";
		}

		// Change la couleur si l'abonnement est payé ou pas
		$label_paye = "";

		if ($row['Paye'] == 'Oui'){
			$label_paye = "success";
		}

		if ($row['Paye'] == 'Non'){
			$label_paye = "danger";
		}

		// Formatage de la Date de l'inscription
		$inscription = date("d-m-Y", strtotime($row['Date_inscription']));

		// Calcul de la fin de contrat
		$abonnement = "+{$row['Abonnement']} month";
		$fin_de_contrat = date("d-m-Y", strtotime($abonnement,strtotime($row['Date_inscription'])));

		// Calcul le mois et l'année de la fin de contrat
		$_1_abonnement = $row['Abonnement'] - 1;
		$abonnement2 = "+{$_1_abonnement} month";
		$time_fin_de_contrat = date("d-m-Y", strtotime($abonnement2,strtotime($row['Date_inscription'])));

		// Défini la date du jour
		$aujourdhui = date("d-m-Y");

		// Défini le mois et l'année de la date du jour
		$time_aujourdhui = date("d-m-Y");

		// Change la couleur de fin de contrat si la fin de contrat se termine à la date du jour
		$label_fin_de_contrat = "";

		if (strtotime($time_fin_de_contrat) <= strtotime($time_aujourdhui)){
			$label_fin_de_contrat = "label label-warning";
		}

		if (strtotime($fin_de_contrat) <= strtotime($aujourdhui)){
			$label_fin_de_contrat = "label label-danger";
		}


		// Affiche le tableau
		echo "
			<tr>
				<td>{$row['Prenom']}</td>
	      		<td>{$row['Nom']}</td>
	      		<td>{$row['Adresse']}</td>
	      		<td>{$row['CP']}</td>
	      		<td>{$row['Ville']}</td>
	      		<td>{$row['Telephone']}</td>
	      		<td><a href='mailto:{$row['Adresse_mail']}'>{$row['Adresse_mail']}</a></td>
	      		<td><span class='label label-info'>{$row['Identifiant']}</span></td>
	      		<td><span class='label label-info'>{$row['Mot_de_passe']}</span></td>
	      		<td>{$row['Thi_card']}</td>
	      		<td><span class='label label-{$label}'>{$row['Pack']}</span></td>
	      		<td>{$row['Abonnement']} mois</td>
	      		<td>{$row['Mode_de_paiement']}</td>
	      		<td style='text-align: center;'><span class='label label-{$label_paye}'>{$row['Paye']}</span></td>
	      		<td>{$inscription}</td>
	      		<td><span class='{$label_fin_de_contrat}'>{$fin_de_contrat}</span></td>
	      		<td><form action='gestion_adherents.php' method='post'><input type='hidden' name='renew_adherent' id='renew_adherent' value='{$row['Id']}'><button class='btn btn-info btn-sm fa fa-retweet' type='submit'></button></form></td>
	      		<td><form action='modif_adherent.php' method='post'><input type='hidden' name='modif_adherent' id='modif_adherent' value='{$row['Id']}'><button class='btn btn-info btn-sm fa fa-cog' type='submit'></button></form></td>
				<td><form action='gestion_adherents.php' method='post'><input type='hidden' name='delete_adherent' id='delete_adherent' value='{$row['Id']}'><button class='btn btn-danger btn-sm fa fa-trash' type='submit'></button></form></td>
			</tr>
		";
	}
}

/* Renouvellement du contrat de l'adhérent */
function renew_adherent(){

	global $link;

	if(isset($_POST) && !empty($_POST['renew_adherent'])){

		$sql = mysqli_query($link, "UPDATE tbl_users SET Date_inscription = NOW() WHERE id = '" . $_POST['renew_adherent'] . "'");

		$html = "<script>swal('', 'Le renouvellement du contrat à bien été pris en compte !', 'success');</script>";

		return $html;

	}
}

/* Modification d'un adhérent */
function modif_adherent(){

	global $link;

	if(isset($_POST) && !empty($_POST['form_name']) && !empty($_POST['form_prenom']) && !empty($_POST['form_address']) && !empty($_POST['form_cp']) && !empty($_POST['form_city']) && !empty($_POST['form_telephone']) && !empty($_POST['form_email']) && !empty($_POST['abonnement'])){

		$id 				= $_POST['Numadherent'];
		$nom 				= addslashes($_POST['form_name']);
		$prenom 			= addslashes($_POST['form_prenom']);
		$address 			= addslashes($_POST['form_address']);
		$CP 				= $_POST['form_cp'];
		$city 				= addslashes($_POST['form_city']);
		$telephone 			= $_POST['form_telephone'];
		$email 				= $_POST['form_email'];
		$Mode_de_paiement 	= $_POST['optionsmode'];
		$paye 				= $_POST['optionspaye'];
		$thi 				= $_POST['thi_card'];
		$pack 				= $_POST['optionspack'];
		$abonnement			= $_POST['abonnement'];

		$sql = mysqli_query($link, "UPDATE tbl_users SET Nom = '".$nom."', Prenom = '".$prenom."',  Adresse = '".$address."', CP = '".$CP."', Ville = '".$city."', Telephone = '".$telephone."', Adresse_mail = '".$email."', Mode_de_paiement = '".$Mode_de_paiement."', Paye = '".$paye."', Pack = '".$pack."', Thi_card = '".$thi."', Abonnement = '".$abonnement."' WHERE id = '".$id."'");

		header("location: gestion_adherents.php" );

	}
}

/* Suppression d'un adhérent */
function delete_adherent(){

	global $link;

	if(isset($_POST) && !empty($_POST['delete_adherent'])){

		$sql = mysqli_query($link, "DELETE FROM tbl_users WHERE id = '".$_POST['delete_adherent']."'");

		$html = "<script>swal('', 'L\'adhérent à bien été supprimé !', 'warning');</script>";

		return $html;

	}
}

/* Ajouter un adhérent */
function add_adherent(){

	global $link;

	if(isset($_POST) && !empty($_POST['form_name']) && !empty($_POST['form_lastname']) && !empty($_POST['form_adress']) && !empty($_POST['form_CP']) && !empty($_POST['form_city']) && !empty($_POST['form_phone']) && !empty($_POST['form_mail']) && !empty($_POST['optionspack']) && !empty($_POST['optionsmode']) && !empty($_POST['optionspaye']) && !empty($_POST['abonnement'])){

		$nom 				= addslashes($_POST['form_name']);
		$prenom 			= addslashes($_POST['form_lastname']);
		$adresse 			= addslashes($_POST['form_adress']);
		$CP 				= $_POST['form_CP'];
		$ville 				= addslashes($_POST['form_city']);
		$telephone 			= $_POST['form_phone'];
		$email 				= $_POST['form_mail'];
		$pack 				= $_POST['optionspack'];
		$Mode_de_paiement 	= $_POST['optionsmode'];
		$paye 				= $_POST['optionspaye'];
		$abonnement 		= $_POST['abonnement'];

		// Génération d'un identifiant
		$identifiant = substr($prenom, 0, 3);
		$identifiant .= $nom;

		// Génération d'un mot de passe
		$length = 8;
		$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$count = mb_strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
		        $index = rand(0, $count - 1);
		        $result .= mb_substr($chars, $index, 1);
		    }
		$mot_de_passe = $result;
		
		// Si l'adhérent a un contrat Gold ou Silver
		if($_POST['optionspack'] == 'Silver'){
					
			// Envoi d'un mail à l'adhérent avec son identifiant et mot de passe
			$destinataire = $email;
			$expediteur = 'contact@crossfit-reze.fr';
			$copie = '';
			$copie_cachee = '';
			$objet = 'Crossfit Reze - Votre identifiant & Mot de passe'; // Objet du message
			$headers  = 'MIME-Version: 1.0' . "\n"; // Version MIME
			$headers .= 'Content-type: text/html; charset=ISO-8859-1'."\n"; // l'en-tete Content-type pour le format HTML
			$headers .= 'From: "Crossfit Reze"<'.$expediteur.'>'."\n"; // Expediteur
			$headers .= 'Delivered-to: '.$destinataire."\n"; // Destinataire
			$headers .= 'Cc: '.$copie."\n"; // Copie Cc
			$headers .= 'Bcc: '.$copie_cachee."\n\n"; // Copie cachée Bcc        
			$message = '
			<div style="width: 100%">
				Bonjour '.$prenom.',<br><br><br>
				Veuillez trouver ci-joint votre identifiant & mot de passe qui vous permet d\'accéder à l\'espace adhérent.<br><br><br><br>
				Identifiant: <b>'.$identifiant.'</b><br><br>
				Mot de passe: <b>'.$mot_de_passe.'</b><br><br><br><br>
				Sportivement,<br><br>
				L\'équipe Crossfit Rezé.<br><br><br>
				<p style="color: #9FC31A; font-family: Arial,Helvetica,sans-serif;">
					<img src="https://www.crossfit-reze.fr/assets/img/favicon.png" alt="Crossfit Rezé" width="27" height="27" align="left" border="0" hspace="2" /><span style=" font-size: small;"><strong>Urban Factory</strong></span><br /><span style="font-size: xx-small;"><strong>6 Rue du Seil, 44400 Rezé - 09 80 77 94 17</strong></span>
				</p>
				<p>
					<small>Conformément à la loi "informatique et libertés" du 6 janvier 1978 modifiée, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent.<br>Si vous souhaitez exercer ce droit et obtenir la communication des informations vous concernant, veuillez vous adresser à contact@Urban-factory.fr</small>
				</p>
			</div>';
			mail($destinataire, $objet, $message, $headers); // Envoi du message
		}
		
		// Requete pour ajouter l'adhérent dans la table	
		$rtq = "INSERT INTO tbl_users (Administrateur, Nom, Prenom, Adresse, CP, Ville, Telephone, Adresse_mail, Mode_de_paiement, Paye, Pack, Date_inscription, Identifiant, Mot_de_passe, Abonnement) VALUES ('0','".$nom."','".$prenom."','".$adresse."','".$CP."','".$ville."','".$telephone."','".$email."','".$Mode_de_paiement."','".$paye."','".$pack."',NOW(), '".$identifiant."', '".$mot_de_passe."', '".$abonnement."')";
		$sql = mysqli_query($link,$rtq);
		header("location: gestion_adherents.php" );

	}
}

/* Envoi mail quand la fin du contrat de l'adhérent arrive à terme */
function mail_adherent(){

	global $link;

		$sql = mysqli_query($link, "SELECT Nom, Prenom, Adresse_mail, Date_inscription, Abonnement FROM tbl_users");

		while($row = mysqli_fetch_array($sql)){

			// Calcul de la fin de contrat
			$abonnement = "+{$row['Abonnement']} month";
			$fin_de_contrat = date("d-m-Y", strtotime($abonnement,strtotime($row['Date_inscription'])));

			// Calcul de la date un mois avant la fin de contrat
			$_1_abonnement = $row['Abonnement'] - 1;
			$abonnement2 = "+{$_1_abonnement} month";
			$time_fin_de_contrat = date("d-m-Y", strtotime($abonnement2,strtotime($row['Date_inscription'])));

			// Défini la date du jour
			$aujourdhui = date("d-m-Y");

			// Si le contrat de termine dans un mois envoi un mail
			if($time_fin_de_contrat == $aujourdhui){
				$destinataire = $row['Adresse_mail'];
				$expediteur = 'contact@crossfit-reze.fr';
				$copie = '';
				$copie_cachee = '';
				$objet = 'Crossfit Rezé - Expiration Abonnement'; // Objet du message
				$headers  = 'MIME-Version: 1.0' . "\n"; // Version MIME
				$headers .= 'Content-type: text/html; charset=ISO-8859-1'."\n"; // l'en-tete Content-type pour le format HTML
				$headers .= 'From: "Crossfit Reze"<'.$expediteur.'>'."\n"; // Expediteur
				$headers .= 'Delivered-to: '.$destinataire."\n"; // Destinataire
				$headers .= 'Cc: '.$copie."\n"; // Copie Cc
				$headers .= 'Bcc: '.$copie_cachee."\n\n"; // Copie cachée Bcc        
				$message = '
					<div style="width: 100%">
						Bonjour '.$row['Prenom'].',<br><br><br>
						Votre abonnement se termine dans un mois !<br><br>
						Pensez à le renouveler !</b><br><br>
						Sportivement,<br><br>
						L\'équipe Crossfit Reze.<br><br><br>
						<p style="color: #9FC31A; font-family: Arial,Helvetica,sans-serif;">
							<img src="https://www.crossfit-reze.fr/assets/img/favicon.png" alt="Crossfit Reze" width="27" height="27" align="left" border="0" hspace="2" /><span style=" font-size: small;"><strong>Urban Factory</strong></span><br /><span style="font-size: xx-small;"><strong>6 Rue du Seil, 44400 Rezé - 09 80 77 94 17</strong></span>
						</p>
						<p>
							<small>Conformément à la loi "informatique et libertés" du 6 janvier 1978 modifiée, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent.<br>Si vous souhaitez exercer ce droit et obtenir la communication des informations vous concernant, veuillez vous adresser à contact@Urban-factory.fr</small>
						</p>
					</div>';
				mail($destinataire, $objet, $message, $headers); // Envoi du message
			}

			if($fin_de_contrat == $aujourdhui){
				$destinataire = $row['Adresse_mail'];
				$expediteur = 'contact@Urban-factory.fr';
				$copie = '';
				$copie_cachee = '';
				$objet = 'Urban Factory - Expiration Abonnement'; // Objet du message
				$headers  = 'MIME-Version: 1.0' . "\n"; // Version MIME
				$headers .= 'Content-type: text/html; charset=ISO-8859-1'."\n"; // l'en-tete Content-type pour le format HTML
				$headers .= 'From: "Urban Factory"<'.$expediteur.'>'."\n"; // Expediteur
				$headers .= 'Delivered-to: '.$destinataire."\n"; // Destinataire
				$headers .= 'Cc: '.$copie."\n"; // Copie Cc
				$headers .= 'Bcc: '.$copie_cachee."\n\n"; // Copie cachée Bcc        
				$message = '
					<div style="width: 100%">
						Bonjour '.$row['Prenom'].',<br><br><br>
						Votre abonnement est arrivé à expiration !<br><br>
						Pensez à le renouveler !</b><br><br>
						Sportivement,<br><br>
						L\'équipe Crossfit Reze.<br><br><br>
						<p style="color: #9FC31A; font-family: Arial,Helvetica,sans-serif;">
							<img src="https://www.crossfit-reze.fr/assets/img/favicon.png" alt="Crossfit Reze" width="27" height="27" align="left" border="0" hspace="2" /><span style=" font-size: small;"><strong>Urban Factory</strong></span><br /><span style="font-size: xx-small;"><strong>6 Rue du Seil, 44400 Rezé - 09 80 77 94 17</strong></span>
						</p>
						<p>
							<small>Conformément à la loi "informatique et libertés" du 6 janvier 1978 modifiée, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent.<br>Si vous souhaitez exercer ce droit et obtenir la communication des informations vous concernant, veuillez vous adresser à contact@Urban-factory.fr</small>
						</p>
					</div>';
				mail($destinataire, $objet, $message, $headers); // Envoi du message
			}

		}
}

/* Bouton Export de la liste Adhérent en CSV */
function export_csv(){
	
	global $link;

	if(isset($_POST) && !empty($_POST['export_adherents'])){

		//Premiere ligne = nom des champs (
		$xls_output = "Nom;Prenom;Adresse;CP;Ville;Telephone;Adresse_mail;Pack;Date_inscription";
		$xls_output .= "\n";

		$sql = mysqli_query($link, "SELECT Nom, Prenom, Adresse, CP, Ville, Telephone, Adresse_mail, Pack, Date_inscription FROM tbl_users");

		while($row = mysqli_fetch_array($sql)){
			$xls_output .= "$row[Nom];$row[Prenom];$row[Adresse];$row[CP];$row[Ville];$row[Telephone];$row[Adresse_mail];$row[Pack];$row[Date_inscription]\n";
		}

		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header("Content-disposition: attachment; filename=Urban_liste_adherents_" . date("Ymd").".csv");
		$xls_output = mb_convert_encoding($xls_output, 'UCS-2LE', 'UTF-8');
		print $xls_output;

		exit;

	}
}

/***
 *       ____ _____ ____ _____ ___ ___  _   _   _____ _   _ ___ 
 *      / ___| ____/ ___|_   _|_ _/ _ \| \ | | |_   _| | | |_ _|
 *     | |  _|  _| \___ \ | |  | | | | |  \| |   | | | |_| || | 
 *     | |_| | |___ ___) || |  | | |_| | |\  |   | | |  _  || | 
 *      \____|_____|____/ |_| |___\___/|_| \_|   |_| |_| |_|___|
 *                                                              
 */

/* Reservation des THI */
function reservation_thi(){

	global $link;

	if(isset($_POST) && !empty($_POST['reservation_thi'])){

		// Récuppère le nombre d'adhérents s'étant déjà inscrits
		$sql = mysqli_query($link, "SELECT * FROM tbl_reservations WHERE id_thi = '".$_POST['reservation_thi']."'");
        $verif_limite = mysqli_num_rows($sql);

        // Récuppère la limite maximal du nombre d'adhérent à ce THI
        $sql2 = mysqli_query($link, "SELECT limite FROM tbl_thi WHERE id = '".$_POST['reservation_thi']."'");
        $row = mysqli_fetch_array($sql2);
        $limite = $row['limite'];

       	// Vérifi si l'adhérent est déjà inscrit ou pas
       	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$_POST['reservation_thi']."' AND id_adherent = '".$_SESSION['id']."'");
       	$verif_resa = mysqli_num_rows($sql3);

        $html = "";

        if($verif_limite < $limite){
        	if($verif_resa == 0){

        		$id_thi 			= $_POST['reservation_thi'];
				$id_adherent 		= $_SESSION['id'];
				$nom_adherent 		= $_SESSION['Nom'];
				$prenom_adherent 	= $_SESSION['Prenom'];

				// Récuppère le nombre de séance restante de l'adhérent
		        $sql4 = mysqli_query($link, "SELECT Thi_card, Pack FROM tbl_users WHERE id = '".$id_adherent."'");
		        $row = mysqli_fetch_array($sql4);
		        $thi = $row['Thi_card'];
		        $pack = $row['Pack'];

		        if($pack == "Silver"){

		        	$sql = mysqli_query($link, "INSERT INTO tbl_reservations (nom, prenom, id_adherent, id_thi) VALUES ('".$nom_adherent."', '".$prenom_adherent."', '".$id_adherent."', '".$id_thi."')");
				
					$html = "<script>swal('', 'Votre inscription à bien été prise en compte !', 'success');</script>";

		        }

		        else if($thi > 0 && $pack != "Silver"){

		        	$thi = $thi - 1;

		        	$_SESSION['Thi_card'] = $thi;

					$sql = mysqli_query($link, "INSERT INTO tbl_reservations (nom, prenom, id_adherent, id_thi) VALUES ('".$nom_adherent."', '".$prenom_adherent."', '".$id_adherent."', '".$id_thi."')");

					$sql = mysqli_query($link, "UPDATE tbl_users SET Thi_card = '".$thi."' WHERE id = '" . $id_adherent . "'");
				
					$html = "<script>swal('', 'Votre inscription à bien été prise en compte !', 'success');</script>";

		        }
		        else{
		        	$html = "<script>swal('', 'Désolé mais vous n\'avez plus de séance disponible !', 'error');</script>";
		        }
        	}
        	else{
        		$html = "<script>swal('', 'Désolé mais vous êtes déjà inscrit !', 'error');</script>";
        	}	                
        }
        else{

        	$html = "<script>swal('Désolé mais quelqu\'un a été plus rapide que vous !', 'Le WOD est déjà complet !', 'error');</script>";

        	
        }
    	return $html;		
	}
}

/* Affiche des THI par jour */
function affichage_thi_lundi(){

	global $link;

	/* Réccupère tous les THI du lundi */
	$sql = mysqli_query($link, "SELECT id, jour_de_la_semaine, horaire_debut,horaire_fin,limite FROM tbl_thi WHERE jour_de_la_semaine = 'lundi'");

	/* Récuppère le temps défini ou un THI n'est plus réservable */
	$sql2 = mysqli_query($link, "SELECT timeoff FROM tbl_options");
	$row2 = mysqli_fetch_array($sql2);
    $timeoff = $row2['timeoff'];

    $sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);
    $semaine = $row3['semaine'];

	$nombre_thi = mysqli_num_rows($sql);

	if($nombre_thi >= 1){
		
		while($row = mysqli_fetch_array($sql)){

			$id = $row['id'];
			$joursemaine = $row['jour_de_la_semaine'];
			$horairedebut = $row['horaire_debut'];
			$horairefin = $row['horaire_fin'];
			$limite = $row['limite'];

			/* Requete pour savoir si l'adhérent à déjà réservé */
			$sql2 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$id."' AND id_adherent = '".$_SESSION['id']."'");
          	
          	$result = mysqli_num_rows($sql2);

          	// Si l'adhérent a déjà réservé, retourne l'id de réservation
          	if($result != 0){
          		$pow = mysqli_fetch_array($sql2);
          		$id_reservation = $pow['id'];
          	}

          	/* Requete pour savoir si le THI est complet */
          	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi, nom, prenom FROM tbl_reservations WHERE id_thi = '".$id."'");
          	$verif_limite = mysqli_num_rows($sql3);     	

          	/* Calcul qui permet de faire avancer la barre de progression */
          	$progress_bar = round(($verif_limite * 100) / $limite);

          	/* Calcul qui permet de savoir si le temps imparti pour réservé est dépassé ou pas */
      		/* Lundi de chaque semaine */
			$l = strtotime(date('o-\\W'.$semaine.''));
			$lprim = date('d M Y',$l);
          	$b = strtotime($lprim.$horairedebut.':00');
          	$c = "-";
			$c .= $timeoff;
			$c .= "hour";
			$timeover = strtotime(date('Y-m-d H:i:s', strtotime($c, $b)));
			$now = strtotime("now");

          	////////////////////////////////////////////////////////////////////////////////

			echo "
			<tr>
          		<td>{$horairedebut} - {$horairefin}</td>
          		<td>WOD</td>
          		<td>
          			{$verif_limite} sur {$limite}
            		<div class='progress progress-xs progress-striped active'>
              			<div class='progress-bar progress-bar-primary' style='width: {$progress_bar}%'></div>
            		</div>
          		</td>";

          	// Si la personne connectée est Admin, affiche les adhérents ayant réservés //
  			if(($_SESSION['Administrateur'] > 0)){

  				echo "<td>";
  				while($pow = mysqli_fetch_array($sql3)){
          			$nom_reservation = $pow['nom'];
          			$prenom_reservation = $pow['prenom'];

          			echo "{$prenom_reservation} {$nom_reservation}<br>";
          		}
          		echo "</td>";
          	}

      		/* Vérification si l'adhérent à déjà réservé ce THI et/ou si le THI est complet */
          	if($result == 0){
          		/* Vérifi si le THI est encore réservable au niveau du temps */
          		if($now < $timeover){
	          		if($verif_limite < $limite){
	          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='reservation_thi' id='reservation_thi' value='{$row['id']}'><button class='btn btn-success btn-sm' type='submit'> Réserver</button></form></td>";
	          		}
	          		else{
	          			echo "<td><span class='label label-danger'>WOD complet !</span></td>";
	          		}
	          	}
          		else{
		        	echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
		        }
          		
          	}
          	if($result != 0){
          		if($now < $b){
          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='deinscrition_thi' id='deinscrition_thi' value='{$id_reservation}'><button class='btn btn-warning btn-sm' type='submit'> Se désinscrire</button></form></td>";
          		}
          		else{
	        		echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
	        	}
          	}
	         	
          	if(($_SESSION['Administrateur'] == 1)){

          		echo "
          		<td><form action='modif_thi.php' method='post'><input type='hidden' name='modif_thi' id='modif_thi' value='{$id}'><button class='btn btn-info btn-sm' type='submit'> Modifier</button></form></td>
          		<td><form action='list_thi.php' method='post'><input type='hidden' name='delete_thi' id='delete_thi' value='{$row['id']}'><button class='btn btn-danger btn-sm' type='submit'> Supprimer</button></form></td>";

          	}

        	echo "</tr>";
		}
	}
	else{
		echo "<h4>Aucun WOD trouvé !</h4>";
	}
}

function affichage_thi_mardi(){

	global $link;

	/* Réccupère tous les THI du lundi */
	$sql = mysqli_query($link, "SELECT id, jour_de_la_semaine, horaire_debut,horaire_fin,limite FROM tbl_thi WHERE jour_de_la_semaine = 'mardi'");

	/* Récuppère le temps défini ou un THI n'est plus réservable */
	$sql2 = mysqli_query($link, "SELECT timeoff FROM tbl_options");
	$row2 = mysqli_fetch_array($sql2);
    $timeoff = $row2['timeoff'];

    $sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);
    $semaine = $row3['semaine'];

	$nombre_thi = mysqli_num_rows($sql);

	if($nombre_thi >= 1){
		
		while($row = mysqli_fetch_array($sql)){

			$id = $row['id'];
			$joursemaine = $row['jour_de_la_semaine'];
			$horairedebut = $row['horaire_debut'];
			$horairefin = $row['horaire_fin'];
			$limite = $row['limite'];

			/* Requete pour savoir si l'adhérent à déjà réservé */
			$sql2 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$id."' AND id_adherent = '".$_SESSION['id']."'");
          	
          	$result = mysqli_num_rows($sql2);

          	// Si l'adhérent a déjà réservé, retourne l'id de réservation
          	if($result != 0){
          		$pow = mysqli_fetch_array($sql2);
          		$id_reservation = $pow['id'];
          	}

          	/* Requete pour savoir si le THI est complet */
          	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi, nom, prenom FROM tbl_reservations WHERE id_thi = '".$id."'");
          	$verif_limite = mysqli_num_rows($sql3);     	

          	/* Calcul qui permet de faire avancer la barre de progression */
          	$progress_bar = round(($verif_limite * 100) / $limite);

          	/* Calcul qui permet de savoir si le temps imparti pour réservé est dépassé ou pas */
      		/* Lundi de chaque semaine */
			$l = strtotime(date('o-\\W'.$semaine.''));
			$l = strtotime('+1 day', $l);
			$lprim = date('d M Y',$l);
          	$b = strtotime($lprim.$horairedebut.':00');
          	$c = "-";
			$c .= $timeoff;
			$c .= "hour";
			$timeover = strtotime(date('Y-m-d H:i:s', strtotime($c, $b)));
			$now = strtotime("now");

          	////////////////////////////////////////////////////////////////////////////////

			echo "
			<tr>
          		<td>{$horairedebut} - {$horairefin}</td>
          		<td>WOD</td>
          		<td>
          			{$verif_limite} sur {$limite}
            		<div class='progress progress-xs progress-striped active'>
              			<div class='progress-bar progress-bar-primary' style='width: {$progress_bar}%'></div>
            		</div>
          		</td>";

          	// Si la personne connectée est Admin, affiche les adhérents ayant réservés //
  			if(($_SESSION['Administrateur'] > 0)){

  				echo "<td>";
  				while($pow = mysqli_fetch_array($sql3)){
          			$nom_reservation = $pow['nom'];
          			$prenom_reservation = $pow['prenom'];

          			echo "{$prenom_reservation} {$nom_reservation}<br>";
          		}
          		echo "</td>";
          	}

      		/* Vérification si l'adhérent à déjà réservé ce THI et/ou si le THI est complet */
          	if($result == 0){
          		/* Vérifi si le THI est encore réservable au niveau du temps */
          		if($now < $timeover){
	          		if($verif_limite < $limite){
	          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='reservation_thi' id='reservation_thi' value='{$row['id']}'><button class='btn btn-success btn-sm' type='submit'> Réserver</button></form></td>";
	          		}
	          		else{
	          			echo "<td><span class='label label-danger'>WOD complet !</span></td>";
	          		}
	          	}
          		else{
		        	echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
		        }
          		
          	}
          	if($result != 0){
          		if($now < $b){
          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='deinscrition_thi' id='deinscrition_thi' value='{$id_reservation}'><button class='btn btn-warning btn-sm' type='submit'> Se désinscrire</button></form></td>";
          		}
          		else{
	        		echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
	        	}
          	}
	         	
          	if(($_SESSION['Administrateur'] == 1)){

          		echo "
          		<td><form action='modif_thi.php' method='post'><input type='hidden' name='modif_thi' id='modif_thi' value='{$id}'><button class='btn btn-info btn-sm' type='submit'> Modifier</button></form></td>
          		<td><form action='list_thi.php' method='post'><input type='hidden' name='delete_thi' id='delete_thi' value='{$row['id']}'><button class='btn btn-danger btn-sm' type='submit'> Supprimer</button></form></td>";

          	}

        	echo "</tr>";
		}
	}
	else{
		echo "<h4>Aucun WOD trouvé !</h4>";
	}
}

function affichage_thi_mercredi(){

	global $link;

	/* Réccupère tous les THI du lundi */
	$sql = mysqli_query($link, "SELECT id, jour_de_la_semaine, horaire_debut,horaire_fin,limite FROM tbl_thi WHERE jour_de_la_semaine = 'mercredi'");

	/* Récuppère le temps défini ou un THI n'est plus réservable */
	$sql2 = mysqli_query($link, "SELECT timeoff FROM tbl_options");
	$row2 = mysqli_fetch_array($sql2);
    $timeoff = $row2['timeoff'];

    $sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);
    $semaine = $row3['semaine'];

	$nombre_thi = mysqli_num_rows($sql);

	if($nombre_thi >= 1){
		
		while($row = mysqli_fetch_array($sql)){

			$id = $row['id'];
			$joursemaine = $row['jour_de_la_semaine'];
			$horairedebut = $row['horaire_debut'];
			$horairefin = $row['horaire_fin'];
			$limite = $row['limite'];

			/* Requete pour savoir si l'adhérent à déjà réservé */
			$sql2 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$id."' AND id_adherent = '".$_SESSION['id']."'");
          	
          	$result = mysqli_num_rows($sql2);

          	// Si l'adhérent a déjà réservé, retourne l'id de réservation
          	if($result != 0){
          		$pow = mysqli_fetch_array($sql2);
          		$id_reservation = $pow['id'];
          	}

          	/* Requete pour savoir si le THI est complet */
          	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi, nom, prenom FROM tbl_reservations WHERE id_thi = '".$id."'");
          	$verif_limite = mysqli_num_rows($sql3);     	

          	/* Calcul qui permet de faire avancer la barre de progression */
          	$progress_bar = round(($verif_limite * 100) / $limite);

          	/* Calcul qui permet de savoir si le temps imparti pour réservé est dépassé ou pas */
      		/* Lundi de chaque semaine */
			$l = strtotime(date('o-\\W'.$semaine.''));
			$l = strtotime('+2 day', $l);
			$lprim = date('d M Y',$l);
          	$b = strtotime($lprim.$horairedebut.':00');
          	$c = "-";
			$c .= $timeoff;
			$c .= "hour";
			$timeover = strtotime(date('Y-m-d H:i:s', strtotime($c, $b)));
			$now = strtotime("now");

          	////////////////////////////////////////////////////////////////////////////////

			echo "
			<tr>
          		<td>{$horairedebut} - {$horairefin}</td>
          		<td>WOD</td>
          		<td>
          			{$verif_limite} sur {$limite}
            		<div class='progress progress-xs progress-striped active'>
              			<div class='progress-bar progress-bar-primary' style='width: {$progress_bar}%'></div>
            		</div>
          		</td>";

          	// Si la personne connectée est Admin, affiche les adhérents ayant réservés //
  			if(($_SESSION['Administrateur'] > 0)){

  				echo "<td>";
  				while($pow = mysqli_fetch_array($sql3)){
          			$nom_reservation = $pow['nom'];
          			$prenom_reservation = $pow['prenom'];

          			echo "{$prenom_reservation} {$nom_reservation}<br>";
          		}
          		echo "</td>";
          	}

      		/* Vérification si l'adhérent à déjà réservé ce THI et/ou si le THI est complet */
          	if($result == 0){
          		/* Vérifi si le THI est encore réservable au niveau du temps */
          		if($now < $timeover){
	          		if($verif_limite < $limite){
	          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='reservation_thi' id='reservation_thi' value='{$row['id']}'><button class='btn btn-success btn-sm' type='submit'> Réserver</button></form></td>";
	          		}
	          		else{
	          			echo "<td><span class='label label-danger'>WOD complet !</span></td>";
	          		}
	          	}
          		else{
		        	echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
		        }
          		
          	}
          	if($result != 0){
          		if($now < $timeover){
          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='deinscrition_thi' id='deinscrition_thi' value='{$id_reservation}'><button class='btn btn-warning btn-sm' type='submit'> Se désinscrire</button></form></td>";
          		}
          		else{
	        		echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
	        	}
          	}
	         	
          	if(($_SESSION['Administrateur'] == 1)){

          		echo "
          		<td><form action='modif_thi.php' method='post'><input type='hidden' name='modif_thi' id='modif_thi' value='{$id}'><button class='btn btn-info btn-sm' type='submit'> Modifier</button></form></td>
          		<td><form action='list_thi.php' method='post'><input type='hidden' name='delete_thi' id='delete_thi' value='{$row['id']}'><button class='btn btn-danger btn-sm' type='submit'> Supprimer</button></form></td>";

          	}

        	echo "</tr>";
		}
	}
	else{
		echo "<h4>Aucun WOD trouvé !</h4>";
	}
}

function affichage_thi_jeudi(){

	global $link;

	/* Réccupère tous les THI du lundi */
	$sql = mysqli_query($link, "SELECT id, jour_de_la_semaine, horaire_debut,horaire_fin,limite FROM tbl_thi WHERE jour_de_la_semaine = 'jeudi'");

	/* Récuppère le temps défini ou un THI n'est plus réservable */
	$sql2 = mysqli_query($link, "SELECT timeoff FROM tbl_options");
	$row2 = mysqli_fetch_array($sql2);
    $timeoff = $row2['timeoff'];

    $sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);
    $semaine = $row3['semaine'];

	$nombre_thi = mysqli_num_rows($sql);

	if($nombre_thi >= 1){
		
		while($row = mysqli_fetch_array($sql)){

			$id = $row['id'];
			$joursemaine = $row['jour_de_la_semaine'];
			$horairedebut = $row['horaire_debut'];
			$horairefin = $row['horaire_fin'];
			$limite = $row['limite'];

			/* Requete pour savoir si l'adhérent à déjà réservé */
			$sql2 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$id."' AND id_adherent = '".$_SESSION['id']."'");
          	
          	$result = mysqli_num_rows($sql2);

          	// Si l'adhérent a déjà réservé, retourne l'id de réservation
          	if($result != 0){
          		$pow = mysqli_fetch_array($sql2);
          		$id_reservation = $pow['id'];
          	}

          	/* Requete pour savoir si le THI est complet */
          	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi, nom, prenom FROM tbl_reservations WHERE id_thi = '".$id."'");
          	$verif_limite = mysqli_num_rows($sql3);     	

          	/* Calcul qui permet de faire avancer la barre de progression */
          	$progress_bar = round(($verif_limite * 100) / $limite);

          	/* Calcul qui permet de savoir si le temps imparti pour réservé est dépassé ou pas */
      		/* Lundi de chaque semaine */
			$l = strtotime(date('o-\\W'.$semaine.''));
			$l = strtotime('+3 day', $l);
			$lprim = date('d M Y',$l);
          	$b = strtotime($lprim.$horairedebut.':00');
          	$c = "-";
			$c .= $timeoff;
			$c .= "hour";
			$timeover = strtotime(date('Y-m-d H:i:s', strtotime($c, $b)));
			$now = strtotime("now");

          	////////////////////////////////////////////////////////////////////////////////

			echo "
			<tr>
          		<td>{$horairedebut} - {$horairefin}</td>
          		<td>WOD</td>
          		<td>
          			{$verif_limite} sur {$limite}
            		<div class='progress progress-xs progress-striped active'>
              			<div class='progress-bar progress-bar-primary' style='width: {$progress_bar}%'></div>
            		</div>
          		</td>";

          	// Si la personne connectée est Admin, affiche les adhérents ayant réservés //
  			if(($_SESSION['Administrateur'] > 0)){

  				echo "<td>";
  				while($pow = mysqli_fetch_array($sql3)){
          			$nom_reservation = $pow['nom'];
          			$prenom_reservation = $pow['prenom'];

          			echo "{$prenom_reservation} {$nom_reservation}<br>";
          		}
          		echo "</td>";
          	}

      		/* Vérification si l'adhérent à déjà réservé ce THI et/ou si le THI est complet */
          	if($result == 0){
          		/* Vérifi si le THI est encore réservable au niveau du temps */
          		if($now < $timeover){
	          		if($verif_limite < $limite){
	          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='reservation_thi' id='reservation_thi' value='{$row['id']}'><button class='btn btn-success btn-sm' type='submit'> Réserver</button></form></td>";
	          		}
	          		else{
	          			echo "<td><span class='label label-danger'>WOD complet !</span></td>";
	          		}
	          	}
          		else{
		        	echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
		        }
          		
          	}
          	if($result != 0){
          		if($now < $b){
          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='deinscrition_thi' id='deinscrition_thi' value='{$id_reservation}'><button class='btn btn-warning btn-sm' type='submit'> Se désinscrire</button></form></td>";
          		}
          		else{
	        		echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
	        	}
          	}
	         	
          	if(($_SESSION['Administrateur'] == 1)){

          		echo "
          		<td><form action='modif_thi.php' method='post'><input type='hidden' name='modif_thi' id='modif_thi' value='{$id}'><button class='btn btn-info btn-sm' type='submit'> Modifier</button></form></td>
          		<td><form action='list_thi.php' method='post'><input type='hidden' name='delete_thi' id='delete_thi' value='{$row['id']}'><button class='btn btn-danger btn-sm' type='submit'> Supprimer</button></form></td>";

          	}

        	echo "</tr>";
		}
	}
	else{
		echo "<h4>Aucun WOD trouvé !</h4>";
	}
}

function affichage_thi_vendredi(){

	global $link;

	/* Réccupère tous les THI du lundi */
	$sql = mysqli_query($link, "SELECT id, jour_de_la_semaine, horaire_debut,horaire_fin,limite FROM tbl_thi WHERE jour_de_la_semaine = 'vendredi'");

	/* Récuppère le temps défini ou un THI n'est plus réservable */
	$sql2 = mysqli_query($link, "SELECT timeoff FROM tbl_options");
	$row2 = mysqli_fetch_array($sql2);
    $timeoff = $row2['timeoff'];

    $sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);
    $semaine = $row3['semaine'];

	$nombre_thi = mysqli_num_rows($sql);

	if($nombre_thi >= 1){
		
		while($row = mysqli_fetch_array($sql)){

			$id = $row['id'];
			$joursemaine = $row['jour_de_la_semaine'];
			$horairedebut = $row['horaire_debut'];
			$horairefin = $row['horaire_fin'];
			$limite = $row['limite'];

			/* Requete pour savoir si l'adhérent à déjà réservé */
			$sql2 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$id."' AND id_adherent = '".$_SESSION['id']."'");
          	
          	$result = mysqli_num_rows($sql2);

          	// Si l'adhérent a déjà réservé, retourne l'id de réservation
          	if($result != 0){
          		$pow = mysqli_fetch_array($sql2);
          		$id_reservation = $pow['id'];
          	}

          	/* Requete pour savoir si le THI est complet */
          	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi, nom, prenom FROM tbl_reservations WHERE id_thi = '".$id."'");
          	$verif_limite = mysqli_num_rows($sql3);     	

          	/* Calcul qui permet de faire avancer la barre de progression */
          	$progress_bar = round(($verif_limite * 100) / $limite);

          	/* Calcul qui permet de savoir si le temps imparti pour réservé est dépassé ou pas */
      		/* Lundi de chaque semaine */
			$l = strtotime(date('o-\\W'.$semaine.''));
			$l = strtotime('+4 day', $l);
			$lprim = date('d M Y',$l);
          	$b = strtotime($lprim.$horairedebut.':00');
          	$c = "-";
			$c .= $timeoff;
			$c .= "hour";
			$timeover = strtotime(date('Y-m-d H:i:s', strtotime($c, $b)));
			$now = strtotime("now");

          	////////////////////////////////////////////////////////////////////////////////

			echo "
			<tr>
          		<td>{$horairedebut} - {$horairefin}</td>
          		<td>WOD</td>
          		<td>
          			{$verif_limite} sur {$limite}
            		<div class='progress progress-xs progress-striped active'>
              			<div class='progress-bar progress-bar-primary' style='width: {$progress_bar}%'></div>
            		</div>
          		</td>";

          	// Si la personne connectée est Admin, affiche les adhérents ayant réservés //
  			if(($_SESSION['Administrateur'] > 0)){

  				echo "<td>";
  				while($pow = mysqli_fetch_array($sql3)){
          			$nom_reservation = $pow['nom'];
          			$prenom_reservation = $pow['prenom'];

          			echo "{$prenom_reservation} {$nom_reservation}<br>";
          		}
          		echo "</td>";
          	}

      		/* Vérification si l'adhérent à déjà réservé ce THI et/ou si le THI est complet */
          	if($result == 0){
          		/* Vérifi si le THI est encore réservable au niveau du temps */
          		if($now < $timeover){
	          		if($verif_limite < $limite){
	          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='reservation_thi' id='reservation_thi' value='{$row['id']}'><button class='btn btn-success btn-sm' type='submit'> Réserver</button></form></td>";
	          		}
	          		else{
	          			echo "<td><span class='label label-danger'>WOD complet !</span></td>";
	          		}
	          	}
          		else{
		        	echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
		        }
          		
          	}
          	if($result != 0){
          		if($now < $b){
          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='deinscrition_thi' id='deinscrition_thi' value='{$id_reservation}'><button class='btn btn-warning btn-sm' type='submit'> Se désinscrire</button></form></td>";
          		}
          		else{
	        		echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
	        	}
          	}
	         	
          	if(($_SESSION['Administrateur'] == 1)){

          		echo "
          		<td><form action='modif_thi.php' method='post'><input type='hidden' name='modif_thi' id='modif_thi' value='{$id}'><button class='btn btn-info btn-sm' type='submit'> Modifier</button></form></td>
          		<td><form action='list_thi.php' method='post'><input type='hidden' name='delete_thi' id='delete_thi' value='{$row['id']}'><button class='btn btn-danger btn-sm' type='submit'> Supprimer</button></form></td>";

          	}

        	echo "</tr>";
		}
	}
	else{
		echo "<h4>Aucun WOD trouvé !</h4>";
	}
}

function affichage_thi_samedi(){

	global $link;

	/* Réccupère tous les THI du lundi */
	$sql = mysqli_query($link, "SELECT id, jour_de_la_semaine, horaire_debut,horaire_fin,limite FROM tbl_thi WHERE jour_de_la_semaine = 'samedi'");

	/* Récuppère le temps défini ou un THI n'est plus réservable */
	$sql2 = mysqli_query($link, "SELECT timeoff FROM tbl_options");
	$row2 = mysqli_fetch_array($sql2);
    $timeoff = $row2['timeoff'];

    $sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);
    $semaine = $row3['semaine'];

	$nombre_thi = mysqli_num_rows($sql);

	if($nombre_thi >= 1){
		
		while($row = mysqli_fetch_array($sql)){

			$id = $row['id'];
			$joursemaine = $row['jour_de_la_semaine'];
			$horairedebut = $row['horaire_debut'];
			$horairefin = $row['horaire_fin'];
			$limite = $row['limite'];

			/* Requete pour savoir si l'adhérent à déjà réservé */
			$sql2 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$id."' AND id_adherent = '".$_SESSION['id']."'");
          	
          	$result = mysqli_num_rows($sql2);

          	// Si l'adhérent a déjà réservé, retourne l'id de réservation
          	if($result != 0){
          		$pow = mysqli_fetch_array($sql2);
          		$id_reservation = $pow['id'];
          	}

          	/* Requete pour savoir si le THI est complet */
          	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi, nom, prenom FROM tbl_reservations WHERE id_thi = '".$id."'");
          	$verif_limite = mysqli_num_rows($sql3);     	

          	/* Calcul qui permet de faire avancer la barre de progression */
          	$progress_bar = round(($verif_limite * 100) / $limite);

          	/* Calcul qui permet de savoir si le temps imparti pour réservé est dépassé ou pas */
      		/* Lundi de chaque semaine */
			$l = strtotime(date('o-\\W'.$semaine.''));
			$l = strtotime('+5 day', $l);
			$lprim = date('d M Y',$l);
          	$b = strtotime($lprim.$horairedebut.':00');
          	$c = "-";
			$c .= $timeoff;
			$c .= "hour";
			$timeover = strtotime(date('Y-m-d H:i:s', strtotime($c, $b)));
			$now = strtotime("now");

          	////////////////////////////////////////////////////////////////////////////////

			echo "
			<tr>
          		<td>{$horairedebut} - {$horairefin}</td>
          		<td>WOD</td>
          		<td>
          			{$verif_limite} sur {$limite}
            		<div class='progress progress-xs progress-striped active'>
              			<div class='progress-bar progress-bar-primary' style='width: {$progress_bar}%'></div>
            		</div>
          		</td>";

          	// Si la personne connectée est Admin, affiche les adhérents ayant réservés //
  			if(($_SESSION['Administrateur'] > 0)){

  				echo "<td>";
  				while($pow = mysqli_fetch_array($sql3)){
          			$nom_reservation = $pow['nom'];
          			$prenom_reservation = $pow['prenom'];

          			echo "{$prenom_reservation} {$nom_reservation}<br>";
          		}
          		echo "</td>";
          	}

      		/* Vérification si l'adhérent à déjà réservé ce THI et/ou si le THI est complet */
          	if($result == 0){
          		/* Vérifi si le THI est encore réservable au niveau du temps */
          		if($now < $timeover){
	          		if($verif_limite < $limite){
	          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='reservation_thi' id='reservation_thi' value='{$row['id']}'><button class='btn btn-success btn-sm' type='submit'> Réserver</button></form></td>";
	          		}
	          		else{
	          			echo "<td><span class='label label-danger'>WOD complet !</span></td>";
	          		}
	          	}
          		else{
		        	echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
		        }
          		
          	}
          	if($result != 0){
          		if($now < $b){
          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='deinscrition_thi' id='deinscrition_thi' value='{$id_reservation}'><button class='btn btn-warning btn-sm' type='submit'> Se désinscrire</button></form></td>";
          		}
          		else{
	        		echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
	        	}
          	}


	         	
          	if(($_SESSION['Administrateur'] == 1)){

          		echo "
          		<td><form action='modif_thi.php' method='post'><input type='hidden' name='modif_thi' id='modif_thi' value='{$id}'><button class='btn btn-info btn-sm' type='submit'> Modifier</button></form></td>
          		<td><form action='list_thi.php' method='post'><input type='hidden' name='delete_thi' id='delete_thi' value='{$row['id']}'><button class='btn btn-danger btn-sm' type='submit'> Supprimer</button></form></td>";

          	}

        	echo "</tr>";
		}
	}
	else{
		echo "<h4>Aucun WOD trouvé !</h4>";
	}
}

function affichage_thi_dimanche(){

	global $link;

	/* Réccupère tous les THI du lundi */
	$sql = mysqli_query($link, "SELECT id, jour_de_la_semaine, horaire_debut,horaire_fin,limite FROM tbl_thi WHERE jour_de_la_semaine = 'dimanche'");

	/* Récuppère le temps défini ou un THI n'est plus réservable */
	$sql2 = mysqli_query($link, "SELECT timeoff FROM tbl_options");
	$row2 = mysqli_fetch_array($sql2);
    $timeoff = $row2['timeoff'];

    $sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);
    $semaine = $row3['semaine'];

	$nombre_thi = mysqli_num_rows($sql);

	if($nombre_thi >= 1){
		
		while($row = mysqli_fetch_array($sql)){

			$id = $row['id'];
			$joursemaine = $row['jour_de_la_semaine'];
			$horairedebut = $row['horaire_debut'];
			$horairefin = $row['horaire_fin'];
			$limite = $row['limite'];

			/* Requete pour savoir si l'adhérent à déjà réservé */
			$sql2 = mysqli_query($link, "SELECT id, id_adherent, id_thi FROM tbl_reservations WHERE id_thi = '".$id."' AND id_adherent = '".$_SESSION['id']."'");
          	
          	$result = mysqli_num_rows($sql2);

          	// Si l'adhérent a déjà réservé, retourne l'id de réservation
          	if($result != 0){
          		$pow = mysqli_fetch_array($sql2);
          		$id_reservation = $pow['id'];
          	}

          	/* Requete pour savoir si le THI est complet */
          	$sql3 = mysqli_query($link, "SELECT id, id_adherent, id_thi, nom, prenom FROM tbl_reservations WHERE id_thi = '".$id."'");
          	$verif_limite = mysqli_num_rows($sql3);     	

          	/* Calcul qui permet de faire avancer la barre de progression */
          	$progress_bar = round(($verif_limite * 100) / $limite);

          	/* Calcul qui permet de savoir si le temps imparti pour réservé est dépassé ou pas */
      		/* Lundi de chaque semaine */
			$l = strtotime(date('o-\\W'.$semaine.''));
			$l = strtotime('+6 day', $l);
			$lprim = date('d M Y',$l);
          	$b = strtotime($lprim.$horairedebut.':00');
          	$c = "-";
			$c .= $timeoff;
			$c .= "hour";
			$timeover = strtotime(date('Y-m-d H:i:s', strtotime($c, $b)));
			$now = strtotime("now");

          	////////////////////////////////////////////////////////////////////////////////

			echo "
			<tr>
          		<td>{$horairedebut} - {$horairefin}</td>
          		<td>WOD</td>
          		<td>
          			{$verif_limite} sur {$limite}
            		<div class='progress progress-xs progress-striped active'>
              			<div class='progress-bar progress-bar-primary' style='width: {$progress_bar}%'></div>
            		</div>
          		</td>";

          	// Si la personne connectée est Admin, affiche les adhérents ayant réservés //
  			if(($_SESSION['Administrateur'] > 0)){

  				echo "<td>";
  				while($pow = mysqli_fetch_array($sql3)){
          			$nom_reservation = $pow['nom'];
          			$prenom_reservation = $pow['prenom'];

          			echo "{$prenom_reservation} {$nom_reservation}<br>";
          		}
          		echo "</td>";
          	}

      		/* Vérification si l'adhérent à déjà réservé ce THI et/ou si le THI est complet */
          	if($result == 0){
          		/* Vérifi si le THI est encore réservable au niveau du temps */
          		if($now < $timeover){
	          		if($verif_limite < $limite){
	          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='reservation_thi' id='reservation_thi' value='{$row['id']}'><button class='btn btn-success btn-sm' type='submit'> Réserver</button></form></td>";
	          		}
	          		else{
	          			echo "<td><span class='label label-danger'>WOD complet !</span></td>";
	          		}
	          	}
          		else{
		        	echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
		        }
          		
          	}
          	if($result != 0){
          		if($now < $b){
          			echo "<td><form action='list_thi.php' method='post'><input type='hidden' name='deinscrition_thi' id='deinscrition_thi' value='{$id_reservation}'><button class='btn btn-warning btn-sm' type='submit'> Se désinscrire</button></form></td>";
          		}
          		else{
	        		echo "<td><span class='label label-danger'>Vous ne pouvez plus reserver ce WOD !</span></td>";
	        	}
          	}
	         	
          	if(($_SESSION['Administrateur'] == 1)){

          		echo "
          		<td><form action='modif_thi.php' method='post'><input type='hidden' name='modif_thi' id='modif_thi' value='{$id}'><button class='btn btn-info btn-sm' type='submit'> Modifier</button></form></td>
          		<td><form action='list_thi.php' method='post'><input type='hidden' name='delete_thi' id='delete_thi' value='{$row['id']}'><button class='btn btn-danger btn-sm' type='submit'> Supprimer</button></form></td>";

          	}

        	echo "</tr>";
		}
	}
	else{
		echo "<h4>Aucun WOD trouvé !</h4>";
	}
}

/* Ajout d'un THI en BDD */
function add_thi(){

	global $link, $nombre_thi;

	if(isset($_POST) && !empty($_POST['jour_semaine']) && !empty($_POST['horaire_debut']) && !empty($_POST['horaire_fin']) && !empty($_POST['limite'])){
		
		$joursemaine 	= $_POST['jour_semaine'];
		$horairedebut 	= $_POST['horaire_debut'];
		$horairefin 	= $_POST['horaire_fin'];
		$limite 		= $_POST['limite'];

		$sql = mysqli_query($link,"INSERT INTO tbl_thi (jour_de_la_semaine,horaire_debut,horaire_fin,limite) VALUES ('".$joursemaine."','".$horairedebut."','".$horairefin."','".$limite."')");
	
		header("location: list_thi.php" );
	}
}

/* Modification d'un THI */
function modif_thi(){

	global $link;

	if(isset($_POST) && !empty($_POST['jour_semaine']) && !empty($_POST['horaire_debut']) && !empty($_POST['horaire_fin']) && !empty($_POST['limite'])){

		$id 				= $_POST['num_thi'];
		$jourdelasemaine 	= $_POST['jour_semaine'];
		$horairedebut 		= $_POST['horaire_debut'];
		$horairefin 		= $_POST['horaire_fin'];
		$limite 			= $_POST['limite'];

		$sql = mysqli_query($link, "UPDATE tbl_thi SET jour_de_la_semaine = '".$jourdelasemaine."', horaire_debut = '".$horairedebut."',  horaire_fin = '".$horairefin."', limite = '".$limite."' WHERE id = '".$id."'");

		header("location: list_thi.php" );

	}
}

/* Suppression d'un THI */
function delete_thi(){

	global $link;

	$supprm_thi = "";

	if(isset($_POST) && !empty($_POST['delete_thi'])){

		$sql = mysqli_query($link, "DELETE FROM tbl_thi WHERE id = '".$_POST['delete_thi']."'");
		$sql2 = mysqli_query($link, "DELETE FROM tbl_reservations WHERE id_thi = '".$_POST['delete_thi']."'");

		$supprm_thi = "<script>swal('', 'Le WOD à bien été supprimé !', 'warning');</script>";

	}

	return $supprm_thi;
}

/* Désinscription d'un THI */
function deinscription_thi(){

	global $link;

	$pop_up = "";

	if(isset($_POST) && !empty($_POST['deinscrition_thi'])){

		$sql = mysqli_query($link, "DELETE FROM tbl_reservations WHERE id = '".$_POST['deinscrition_thi']."'");

		$id_adherent 		= $_SESSION['id'];
		
		// Récuppère le nombre de séance restante de l'adhérent
        $sql4 = mysqli_query($link, "SELECT Thi_card, Pack FROM tbl_users WHERE id = '".$id_adherent."'");
        $row = mysqli_fetch_array($sql4);
        $thi = $row['Thi_card'];
        $pack = $row['Pack'];

        if($pack != "Silver"){

			$thi = $thi + 1;

			$_SESSION['Thi_card'] = $thi;

			$sql = mysqli_query($link, "UPDATE tbl_users SET Thi_card = '".$thi."' WHERE id = '" . $id_adherent . "'");

		}

		$pop_up = "<script>swal('', 'Votre réservation à bien été annulée !', 'warning');</script>";

	}

	return $pop_up;
}

/* Réinitialiser les compteurs */
function reinitialisation_thi(){

	global $link;

	$sql = mysqli_query($link, "DELETE FROM tbl_reservations");

	$reinitcompteur = "<script>swal('', 'La réinitialisation des compteurs à bien été effectuée !', 'warning');</script>";
}

/* Mise à jour du numéro de la semaine */
function maj_semaine(){

	global $link;

	$sql3 = mysqli_query($link, "SELECT semaine FROM tbl_options");
	$row3 = mysqli_fetch_array($sql3);

	$semaine = $row3['semaine'] + 1;

	if(date('dm') == '3112'){
		$sql = mysqli_query($link, "UPDATE tbl_options SET semaine = '01'");
	}
	else{
		$sql = mysqli_query($link, "UPDATE tbl_options SET semaine = '".$semaine."'");
	}
}


/* TEST */
/* Envoi mail quand la fin du contrat de l'adhérent arrive à terme */
function test_cron(){

	$destinataire = 'antoinee.guerin@gmail.com';
	$expediteur = 'contact@crossfit-reze.fr';
	$copie = '';
	$copie_cachee = '';
	$objet = 'Crossfit Rezé - Expiration Abonnement'; // Objet du message
	$headers  = 'MIME-Version: 1.0' . "\n"; // Version MIME
	$headers .= 'Content-type: text/html; charset=ISO-8859-1'."\n"; // l'en-tete Content-type pour le format HTML
	$headers .= 'From: "Crossfit Reze"<'.$expediteur.'>'."\n"; // Expediteur
	$headers .= 'Delivered-to: '.$destinataire."\n"; // Destinataire
	$headers .= 'Cc: '.$copie."\n"; // Copie Cc
	$headers .= 'Bcc: '.$copie_cachee."\n\n"; // Copie cachée Bcc        
	$message = '
		<div style="width: 100%">
			test
		</div>';
	mail($destinataire, $objet, $message, $headers); // Envoi du message
			
}

?>