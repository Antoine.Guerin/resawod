<?php

include("fonctions.php");

session_start();

if(!isset($_SESSION['login']))
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

if($_SESSION['Administrateur'] == 0)
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

add_adherent();

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $chrg_header; ?>
</head>

	<div class="wrapper">

  		<!-- Main Header -->
		<header class="main-header">
		    <!-- Logo -->
		    <a href="list_thi.php" class="logo">
		    	<!-- mini logo for sidebar mini 50x50 pixels -->
		    	<span class="logo-mini"><b>C</b>R</span>
		    	<!-- logo for regular state and mobile devices -->
		    	<span class="logo-lg"><b>Crossfit</b> Reze</span>
		    </a>
			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">	
	  					<!-- User Account Menu -->
	  					<li class="dropdown user user-menu">
	    					<!-- Menu Toggle Button -->
	    					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      						<!-- The user image in the navbar-->
	      						<img src="dist/img/avatarr.png" class="user-image" alt="User Image">
	      						<!-- hidden-xs hides the username on small devices so only the image appears. -->
	      						<span class="hidden-xs"><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></span>
	    					</a>
	    					<ul class="dropdown-menu">
	      						<!-- The user image in the menu -->
	      						<li class="user-header">
	        						<img src="dist/img/avatarr.png" class="img-circle" alt="User Image">
	        						<p>
	          							<?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?>
	          							<small>Inscrit depuis le <?php echo date("d-m-Y", strtotime($_SESSION['Date_inscription'])); ?></small>
                          <small>Nombre de séance WOD: <?php echo $_SESSION['Thi_card'];?></small>
	        						</p>
	      						</li>
	      						<!-- Menu Footer-->
	      						<li class="user-footer">
	        						<div class="pull-right">
	          							<a href="logout.php" class="btn btn-default btn-flat">Se déconnecter</a>
	        						</div>
	      						</li>
	    					</ul>
	  					</li>
					</ul>
				</div>
			</nav>
		</header>

  		<!-- Left side column. contains the logo and sidebar -->
  		<aside class="main-sidebar">
    		<!-- sidebar: style can be found in sidebar.less -->
    		<section class="sidebar">
      			<!-- Sidebar user panel (optional) -->
      			<div class="user-panel">
        			<div class="pull-left image">
          				<img src="dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        			</div>
        			<div class="pull-left info">
          				<p><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></p>
          				<!-- Status -->
          				<i class="fa fa-circle text-success"></i> En ligne
        			</div>
      			</div>

      			<!-- Sidebar Menu -->
      			<ul class="sidebar-menu">
        			<li class="header">ESPACE ADHÉRENTS</li>
        			<!-- Optionally, you can add icons to the links -->
        			
        			<?php

        			if(($_SESSION['Administrateur'] == 1)){
        				echo "
        				<li class='treeview active'>
          				<a href='#''><i class='fa fa-link'></i> <span>Gestion Adhérent</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
          					<li><a href='gestion_adherents.php'>Liste des adhérent</a></li>
            				<li class='active'><a href='add_adherent.php'>Ajouter un adhérent</a></li>
          				</ul>
        			</li>
        			<li class='treeview'>
          				<a href='#'><i class='fa fa-link'></i> <span>Gestion WOD</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
            				<li><a href='add_thi.php'>Ajouter un WOD</a></li>
          				</ul>
        			</li>";
        			}
        			
        			?>
        			<li><a href="list_thi.php"><i class="fa fa-link"></i> <span>Réservation WOD</span></a></li>
      			</ul>
      		<!-- /.sidebar-menu -->
    		</section>
    	<!-- /.sidebar -->
  		</aside>

  		<!-- Content Wrapper. Contains page content -->
  		<div class="content-wrapper">
    		<!-- Content Header (Page header) -->
    		<section class="content-header">
      			<h1>
        			Gestion Adhérents
        			<small>Créer, modifier, supprimer un adhérent</small>
      			</h1>
    		</section>

    		<!-- Main content -->
    		<section class="content">
    			<div class="row">
			        <!-- left column -->
			        <div class="col-xs-12">
			        	<!-- general form elements -->
			        	<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Ajouter un adhérent</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" action="add_adherent.php" method="post">
				              	<div class="box-body">
				                	<div class="form-group">
				                		<h4>Nom de famille :</h4>
				                  		<div class="input-group">
				                  			<span class="input-group-addon"><i class="fa fa-user"></i></span>
				                  			<input type="text" class="form-control" name="form_name" id="form_name" placeholder="Nom" required>
				                  		</div>
				                	</div>
				                	<div class="form-group">
				                  		<h4>Prénom :</h4>
				                  		<div class="input-group">
				                  			<span class="input-group-addon"><i class="fa fa-user"></i></span>
				                  			<input type="text" class="form-control" name="form_lastname" id="form_lastname" placeholder="Prénom" required>
				                  		</div>
				                	</div>
				                	<div class="form-group">
				                  		<h4>Adresse :</h4>
				                  		<div class="input-group">
				                  			<span class="input-group-addon"><i class="fa fa-home"></i></span>
				                  			<input type="text" class="form-control" name="form_adress" id="form_adress" placeholder="Adresse" required>
				                  		</div>
				                	</div>
				                	<div class="form-group">
				                  		<h4>CP :</h4>
				                  		<div class="input-group">
				                  			<span class="input-group-addon"><i class="fa fa-home"></i></span>
				                  			<input type="number" class="form-control" name="form_CP" id="form_CP" placeholder="Code Postal" required>
				                  		</div>
				                	</div>
				                	<div class="form-group">
				                  		<h4>Ville :</h4>
				                  		<div class="input-group">
				                  			<span class="input-group-addon"><i class="fa fa-home"></i></span>
				                  			<input type="text" class="form-control" name="form_city" id="form_city" placeholder="Ville" required>
				                  		</div>
				                	</div>
				                	<div class="form-group">
				                  		<h4>Téléphone : <small>(format: xx.xx.xx.xx.xx)</small></h4>
				                  		<div class="input-group">
				                  			<span class="input-group-addon"><i class="fa fa-phone"></i></span>
				                  			<input type="text" class="form-control" name="form_phone" id="form_phone" pattern="^\d{2}.\d{2}.\d{2}.\d{2}.\d{2}$" placeholder="Téléphone" required>
				                  		</div>
				                	</div>
				                	<div class="form-group">
				                  		<h4>Adresse mail :</h4>
				                  		<div class="input-group">
				                  			<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
				                  			<input type="email" class="form-control" name="form_mail" id="form_mail" placeholder="Email" required>
				                  		</div>
				                	</div>
                					<div class="form-group">
                						<h4>Pack :</h4>
                  						<div class="radio">
                    						<label class="label label-success">
                      							<input type="radio" name="optionspack" id="form-bronze" value="Bronze">
                      							Bronze <i class="fa fa-trophy"></i>
                    						</label>
                  						</div>
                  						<div class="radio">
                    						<label class="label label-default">
                      							<input type="radio" name="optionspack" id="form-silver" value="Silver">
                      							Silver <i class="fa fa-trophy"></i>
                    						</label>
                  						</div>
                  						<div class="radio">
                    						<label class="label label-warning">
                      							<input type="radio" name="optionspack" id="form-gold" value="Gold">
                      							Gold <i class="fa fa-trophy"></i>
                    						</label>
                  						</div>
                					</div>
                          <div class="form-group">
                            <h4>Durée de l'abonnements :</h4>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" value="1">
                                    1 mois
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" value="3">
                                    3 mois
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" value="6">
                                    6 mois
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="abonnement" value="12">
                                    12 mois
                                </label>
                              </div>
                          </div>
                					<div class="form-group">
                						<h4>Mode de paiements :</h4>
                  						<div class="radio">
                    						<label>
                      							<input type="radio" name="optionsmode" id="form-cb" value="Carte Bancaire">
                      							Carte Bancaire <i class="fa fa-credit-card"></i>
                    						</label>
                  						</div>
                  						<div class="radio">
                    						<label>
                      							<input type="radio" name="optionsmode" id="form-chq" value="Chèque">
                      							Chèque <i class="fa fa-google-wallet"></i>
                    						</label>
                  						</div>
                  						<div class="radio">
                    						<label>
                      							<input type="radio" name="optionsmode" id="form-especes" value="Espèces">
                      							Espèces <i class="fa fa-money"></i>
                    						</label>
                  						</div>
                  						<div class="radio">
                    						<label>
                      							<input type="radio" name="optionsmode" id="form-paypal" value="Paypal">
                      							Paypal <i class="fa fa-paypal"></i>
                    						</label>
                  						</div>
                					</div>
				                	<div class="form-group">
                						<h4>Abonnement payé ?</h4>
                  						<div class="radio">
                    						<label>
                      							<input type="radio" name="optionspaye" id="form-yes" value="Oui">
                      							Oui
                    						</label>
                  						</div>
                  						<div class="radio">
                    						<label>
                      							<input type="radio" name="optionspaye" id="form-no" value="Non">
                      							Non
                    						</label>
                  						</div>
                					</div>
				              	</div>
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-success">Ajouter <i class="fa fa-user"></i></button>
				              	</div>
				            </form>
				      	</div>
						<!-- /.box -->
			        </div>
			    </div>
    		</section>
    		<!-- /.content -->
  		</div>
  		<!-- /.content-wrapper -->
  		<!-- Main Footer -->
  		<footer class="main-footer">
    		<!-- To the right -->
    		<div class="pull-right hidden-xs">
      			Seul, on est fort. Ensemble, on est invincible !
    		</div>
    		<!-- Default to the left -->
    		<strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.crossfit-reze.fr/">Crossfit Reze</a>.</strong> All rights reserved.
  		</footer>
  		<!-- Add the sidebar's background. This div must be placed
       	immediately after the control sidebar -->
  		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<?php echo $chrg_footer; ?>
</body>
</html>