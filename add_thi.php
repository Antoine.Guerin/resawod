<?php

include("fonctions.php");

session_start();

if(!isset($_SESSION['login']))
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

if($_SESSION['Administrateur'] == 0)
{
  echo "Vous n'êtes pas autorisé à acceder à cette zone<br /><a href='index.php'>Retour</a>";
  exit;
}

add_thi();

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $chrg_header; ?>
</head>

	<div class="wrapper">

  		<!-- Main Header -->
		<header class="main-header">
		    <!-- Logo -->
		    <a href="list_thi.php" class="logo">
		    	<!-- mini logo for sidebar mini 50x50 pixels -->
		    	<span class="logo-mini"><b>C</b>R</span>
		    	<!-- logo for regular state and mobile devices -->
		    	<span class="logo-lg"><b>Crossfit</b> Reze</span>
		    </a>
			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">	
	  					<!-- User Account Menu -->
	  					<li class="dropdown user user-menu">
	    					<!-- Menu Toggle Button -->
	    					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      						<!-- The user image in the navbar-->
	      						<img src="dist/img/avatarr.png" class="user-image" alt="User Image">
	      						<!-- hidden-xs hides the username on small devices so only the image appears. -->
	      						<span class="hidden-xs"><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></span>
	    					</a>
	    					<ul class="dropdown-menu">
	      						<!-- The user image in the menu -->
	      						<li class="user-header">
	        						<img src="dist/img/avatarr.png" class="img-circle" alt="User Image">
	        						<p>
	          							<?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?>
	          							<small>Inscrit depuis le <?php echo date("d-m-Y", strtotime($_SESSION['Date_inscription'])); ?></small>
	          							<small>Nombre de séance WOD: <?php echo $_SESSION['Thi_card'];?></small>
	        						</p>
	      						</li>
	      						<!-- Menu Footer-->
	      						<li class="user-footer">
	        						<div class="pull-right">
	          							<a href="logout.php" class="btn btn-default btn-flat">Se déconnecter</a>
	        						</div>
	      						</li>
	    					</ul>
	  					</li>
					</ul>
				</div>
			</nav>
		</header>

  		<!-- Left side column. contains the logo and sidebar -->
  		<aside class="main-sidebar">
    		<!-- sidebar: style can be found in sidebar.less -->
    		<section class="sidebar">
      			<!-- Sidebar user panel (optional) -->
      			<div class="user-panel">
        			<div class="pull-left image">
          				<img src="dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        			</div>
        			<div class="pull-left info">
          				<p><?php echo $_SESSION['Prenom']."&nbsp;".$_SESSION['Nom']; ?></p>
          				<!-- Status -->
          				<i class="fa fa-circle text-success"></i> En ligne
        			</div>
      			</div>

      			<!-- Sidebar Menu -->
      			<ul class="sidebar-menu">
        			<li class="header">ESPACE ADHÉRENTS</li>
        			<!-- Optionally, you can add icons to the links -->
        			
        			<?php

        			if(($_SESSION['Administrateur'] == 1)){
        				echo "
        				<li class='treeview'>
          				<a href='#''><i class='fa fa-link'></i> <span>Gestion Adhérent</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
          					<li><a href='gestion_adherents.php'>Liste des adhérent</a></li>
            				<li><a href='add_adherent.php'>Ajouter un adhérent</a></li>
          				</ul>
        			</li>
        			<li class='treeview active'>
          				<a href='#'><i class='fa fa-link'></i> <span>Gestion WOD</span>
            				<span class='pull-right-container'>
              					<i class='fa fa-angle-left pull-right'></i>
            				</span>
          				</a>
          				<ul class='treeview-menu'>
            				<li class='active'><a href='add_thi.php'>Ajouter un WOD</a></li>
          				</ul>
        			</li>";
        			}
        			
        			?>
        			<li><a href="list_thi.php"><i class="fa fa-link"></i> <span>Réservation WOD</span></a></li>
      			</ul>
      		<!-- /.sidebar-menu -->
    		</section>
    	<!-- /.sidebar -->
  		</aside>

  		<!-- Content Wrapper. Contains page content -->
  		<div class="content-wrapper">
    		<!-- Content Header (Page header) -->
    		<section class="content-header">
      			<h1>
        			Gestion WOD
        			<small>Créer, modifier, supprimer un WOD</small>
      			</h1>
    		</section>

    		<!-- Main content -->
    		<section class="content">
    			<div class="row">
			        <!-- left column -->
			        <div class="col-xs-12">
			        	<!-- general form elements -->
			        	<div class="box box-solid box-success">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Ajouter un WOD</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" action="add_thi.php" method="post">
				              	<div class="box-body">
				                	<div class="form-group">
										<h4>Jour de la semaine :</h4>
										<select class="form-control" name="jour_semaine" required>
											<option></option>
											<option>Lundi</option>
											<option>Mardi</option>
											<option>Mercredi</option>
											<option>Jeudi</option>
											<option>Vendredi</option>
											<option>Samedi</option>
											<option>Dimanche</option>
										</select>
									</div>
				                	<div class="form-group">
				                  		<h4>Horaire du début :</h4>
				                  		<select class="form-control" name="horaire_debut" required>
											<option></option>
											<option>7:00</option>
											<option>7:30</option>
											<option>8:00</option>
											<option>8:30</option>
											<option>9:00</option>
											<option>9:30</option>
											<option>10:00</option>
											<option>10:30</option>
											<option>11:00</option>
											<option>11:30</option>
											<option>12:00</option>
											<option>12:15</option>
											<option>12:30</option>
											<option>13:00</option>
											<option>13:15</option>
											<option>13:30</option>
											<option>14:00</option>
											<option>14:30</option>
											<option>15:00</option>
											<option>15:30</option>
											<option>16:00</option>
											<option>16:30</option>
											<option>17:00</option>
											<option>17:30</option>
											<option>18:00</option>
											<option>18:30</option>
											<option>19:00</option>
											<option>19:30</option>
											<option>20:00</option>
											<option>20:30</option>
											<option>21:00</option>
											<option>21:30</option>
											<option>22:00</option>
											<option>22:30</option>
										</select>
				                	</div>
				                	<div class="form-group">
				                  		<h4>Horaire de fin :</h4>
				                  		<select class="form-control" name="horaire_fin" required>
				                  			<option></option>
											<option>7:00</option>
											<option>7:30</option>
											<option>8:00</option>
											<option>8:30</option>
											<option>9:00</option>
											<option>9:30</option>
											<option>10:00</option>
											<option>10:30</option>
											<option>11:00</option>
											<option>11:30</option>
											<option>12:00</option>
											<option>12:15</option>
											<option>12:30</option>
											<option>13:00</option>
											<option>13:15</option>
											<option>13:30</option>
											<option>14:00</option>
											<option>14:30</option>
											<option>15:00</option>
											<option>15:30</option>
											<option>16:00</option>
											<option>16:30</option>
											<option>17:00</option>
											<option>17:30</option>
											<option>18:00</option>
											<option>18:30</option>
											<option>19:00</option>
											<option>19:30</option>
											<option>20:00</option>
											<option>20:30</option>
											<option>21:00</option>
											<option>21:30</option>
											<option>22:00</option>
											<option>22:30</option>
										</select>
				                	</div>
				                	<div class="form-group">
				                		<h4>Inscrire le nombre maximum d'adhérents à ce WOD :</h4>
				                		<input type="number" class="form-control" name="limite" required>
				                	</div>
				              	</div>
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-success">Ajouter <i class="fa fa-plus-circle"></i></button>
				              	</div>
				            </form>
				      	</div>
						<!-- /.box -->
			        </div>
			    </div>
    		</section>
    		<!-- /.content -->
  		</div>
  		<!-- /.content-wrapper -->
  		<!-- Main Footer -->
  		<footer class="main-footer">
    		<!-- To the right -->
    		<div class="pull-right hidden-xs">
      			Seul, on est fort. Ensemble, on est invincible !
    		</div>
    		<!-- Default to the left -->
    		<strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.crossfit-reze.fr/">Crossfit Reze</a>.</strong> All rights reserved.
  		</footer>
  		
  		<!-- Add the sidebar's background. This div must be placed
       	immediately after the control sidebar -->
  		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<?php echo $chrg_footer; ?>
</body>
</html>